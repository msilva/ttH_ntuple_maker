export EOS_MGM_URL=root://eosatlas.cern.ch
export PATH=/afs/cern.ch/project/eos/installation/0.3.84-aquamarine/bin:$PATH
#source /afs/cern.ch/project/eos/installation/atlas/etc/setup.sh
#setupATLAS
#lsetup root 
#source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/5.34.25-x86_64-slc6-gcc48-opt/bin/thisroot.sh
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`pwd`/lib:/afs/cern.ch/sw/lcg/external/Boost/1.55.0_python2.7/x86_64-slc6-gcc47-opt/lib
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Gcc/gcc484_x86_64_slc6/slc6/gcc48/setup.sh
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/5.34.32-HiggsComb-p1-x86_64-slc6-gcc48-opt/bin/thisroot.sh