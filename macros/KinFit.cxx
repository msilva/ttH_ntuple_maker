//--- All hadronic and semileptonic reconstructions
//--- Pluggable functions to compute X2 (and get jets positions reconstructing ttbar)
//-----------------------------------------------------------------------------------------------------

// Needed files
// PyonKinFitSemi.h/cxx - Kinematic fit code
// kfparams_13.dat - parameterisation file


// !!!!! To Add in your main function
//#include "../macros/PyonKinFitSemi.cxx"

// reconstruction function to add to your code - **all hadronic final state**
// m_recoJets: vector of the *TLorentzVector* of all jets passing HGam preselection
// isBTagged: same length as m_recoJets, vector of *int* indicating if corresponding m_recoJets is btagged (1) or not (0) - If you don't want to use it, fill it with 0
// fs: X2 value of the selected jets combination
std::vector< unsigned int > SATTFH(std::vector < int > isBTagged, std::vector< TLorentzVector > m_recoJets, double& fs, PyonKinFitSemi* myAlienKinFit) {

  // vector returned by the function: contains the position in m_recoJets of the selected jets reconstructing ttbar
  // structure: ([0],[1]: W1, [2]: B1), ([3],[4]: W2, [5]: B2)
  std::vector < unsigned int > res;

    unsigned int BBtagged = 0;
    unsigned int LBtagged = 0;
    unsigned int JBtagged = 0;

    // By definition sizes of isBTagged and m_recoJets should be the same
    if (isBTagged.size() != m_recoJets.size()){
        std::cout<<" isBTagged size != recoJets size !! "<<std::endl;
        return res;
    }
    
    for (unsigned int i=0; i<isBTagged.size(); i++){
        if (isBTagged[i]==1) JBtagged++;
    }

    // initial value for X2
    double Best_comb = 999999999999999;

    unsigned int Sel_j1=0, Sel_j2=0, Sel_bh=0, Sel_j3=0, Sel_j4=0, Sel_bl=0;
    unsigned int n_combi=0;

    // condition on nb of jets: most of the events we lose are with njets < 6 so not resolved
    // njets > 11 are very few (~2/3%) and wouldn't have discriminant information
    if ((m_recoJets.size()<6)||(m_recoJets.size()>11)) {
      //std::cout<<" Check number of jets, minimum = 6"<<std::endl;
      //std::cout<<" Jets = "<<m_recoJets.size()<<std::endl;
      fs = 15; // returns a fixed value between converged and not converged 
      return res;
    }
    else{
      std::cout<<" Check number of jets, minimum = 6"<<std::endl;
      std::cout<<" Jets = "<<m_recoJets.size()<<std::endl;
    }

    unsigned int previous_decay_type = myAlienKinFit->GetDecayType(); // check what channel was used before
    if (previous_decay_type != 1) myAlienKinFit->SetDecayType(1); // Set to Fully Hadronic, security if function is called few times in one code

    //how many jets to use?
    //only use 6 for now
    unsigned int n_jets = m_recoJets.size();
    if(n_jets > 6) n_jets = 6;

    // loop over all jets combination
    for (unsigned int i = 0; i < n_jets; i++){ // BH
        TLorentzVector BH = m_recoJets[i];
            
        for (unsigned int j1 = 0; j1 < n_jets; j1++){ // j1
            if (i == j1) continue;
            TLorentzVector J1 = m_recoJets[j1];
            
            for (unsigned int j2=j1+1; j2< n_jets; j2++){ // j2
                if (j2 == i) continue;
                TLorentzVector J2 = m_recoJets[j2];
                
                for (unsigned int k = 0; k < n_jets; k++){ // BL 
                    if (k == j2) continue;
                    if (k == j1) continue;
                    if (k == i) continue;                   
                    
                    TLorentzVector BL = m_recoJets[k];
                    
                    for (unsigned int j3 = 0; j3 < n_jets; j3++){ // j3
                        if (i == j3) continue;
                        if (j1 == j3) continue;
                        if (j2 == j3) continue;
                        if (k == j3) continue;

                        TLorentzVector J3 = m_recoJets[j3];
                        
                        for (unsigned int j4=j3+1; j4< n_jets; j4++){ // j4
                            if (i == j4) continue;
                            if (j1 == j4) continue;
                            if (j2 == j4) continue;
                            if (k == j4) continue;
                            
                            TLorentzVector J4 = m_recoJets[j4];  


                            BBtagged = isBTagged[i]+isBTagged[k];
                            LBtagged = isBTagged[j4]+isBTagged[j3]+isBTagged[j2]+isBTagged[j1];

                            // killing the combination if the only btag is not on BH or BL
                            if (JBtagged == 1) {
                                if ((LBtagged ==1) && (BBtagged == 0)) continue;
                            }
                            // killing the combination if the 2 btags are not on BH and BL
                            if (JBtagged == 2) {
                                if (BBtagged != 2) continue;
                            }
                            //killing the combination if at least 2 btags are not on BH and BL
                            if (JBtagged > 2) {
                                if (BBtagged != 2) continue;
                            }
                            // <-- btag selection only if btag info is given (isBTagged/JBtagged is != 0)

                    
                            n_combi++;     
                    
                                                            
                            // calling KinFit reconstruction (all inputs are converted here in GeV)
                            myAlienKinFit->ReadObjects(J1*(1.0/1000.),J2*(1.0/1000.),BH*(1.0/1000.),J3*(1.0/1000.),J4*(1.0/1000.),BL*(1.0/1000.)); 
                            Int_t State = 0;
                            myAlienKinFit->FitFH(State);

                            double kf_comb = 16;
                            if(State == 1){
			      kf_comb = log(myAlienKinFit->GetKFChi2());
                            } else {
			      // KinFit didn't converge
			      kf_comb=16;
                            }
                            if (kf_comb < Best_comb) { // filling jets position for current best combination (lowest kf_comb)
			      Best_comb = kf_comb;                              
			      Sel_j1 = j1;
			      Sel_j2 = j2;
			      Sel_bh = i;
			      Sel_bl = k;
			      Sel_j3 = j3;
			      Sel_j4 = j4;
                            }
                        }
                    }
                }
            }
        }
    }
    
    // if jets are selected
    if (Sel_j1+Sel_bh+Sel_j3+Sel_j4 > 0) {   
      res.push_back(Sel_j1);
      res.push_back(Sel_j2);
      res.push_back(Sel_bh);               
      res.push_back(Sel_j3);
      res.push_back(Sel_j4);
      res.push_back(Sel_bl);

      //X2 
      fs = Best_comb;
    } else {
        fs=16;
    }
    // put back to whatever it was (can be semileptonic only for the moment, = 0)
    if (previous_decay_type != 1) myAlienKinFit->SetDecayType(previous_decay_type); 
    cout << "log(Chi2): " << fs << endl;
    return res;
}


// ------------
// reconstruction function to add to your code - **semileptonic final state**
// m_recoJets: vector of the *TLorentzVector* of all jets passing HGam preselection
// isBTagged: same length as m_recoJets, vector of *int* indicating if corresponding m_recoJets is btagged (1) or not (0) - If you don't want to use it, fill it with 0
// fs: X2 value of the selected jets combination
std::vector< unsigned int > SATTSemi(std::vector < int > isBTagged, std::vector< TLorentzVector > m_recoJets, std::vector< TLorentzVector > Leptons, double MEx, double MEy, double& fs, unsigned int method) {
    
  // initialisation
  PyonKinFitSemi* myAlienKinFit;
  myAlienKinFit = new PyonKinFitSemi("macros/kfparams_13.dat");
  myAlienKinFit->SetDebugMode(-1); // silent
  // !!!!!
  
    std::vector < unsigned int > res;

    unsigned int BBtagged = 0;
    unsigned int LBtagged = 0;
    unsigned int JBtagged = 0;

    if (isBTagged.size() != m_recoJets.size()){
        std::cout<<" isBTagged size != recoJets size !! "<<std::endl;
        return res;
    }
    
    for (unsigned int i=0; i<isBTagged.size(); i++){
        if (isBTagged[i]==1) JBtagged++;
    }

    // initial value for X2
    double Best_comb = 999999999999999;   
        
    unsigned int Sel_j1=0, Sel_j2=0, Sel_bh=0, Sel_j3=0, Sel_j4=0, Sel_lep=0, Sel_bfakelep=0;;
    unsigned int n_combi=0;
    
    // condition on nb of jets: most of the events we lose are with njets < 4 so not resolved
    // njets > 11 are very few (~1%) and wouldn't have discriminant information
    // Semileptonic ttbar reconstruction so asking exactly 1 lepton
    if ((m_recoJets.size()<4)||(m_recoJets.size()>10)||(Leptons.size()!=1)) {
        std::cout<<" Check number of jets and leptons"<<std::endl;
        std::cout<<" Jets = "<<m_recoJets.size()<<std::endl;
        std::cout<<" Leptons = "<<Leptons.size()<<std::endl;
        fs = 16; // returns a fixed value between converged and not converged 
        return res;
    }
    
    myAlienKinFit->SetDecayType(0); //(0 = semi lept)

    for (unsigned int l=0; l<Leptons.size(); l++){ // Lepton

        if (l>0) continue;
        
        TLorentzVector TL=Leptons[l];
    
    for (unsigned int i = 0; i < m_recoJets.size(); i++){ // BH
        TLorentzVector BH = m_recoJets[i];
        
        for (unsigned int j3 = 0; j3 < m_recoJets.size(); j3++){ // j1
            if (i == j3) continue;
            TLorentzVector J3 = m_recoJets[j3];
            
            for (unsigned int j4=j3+1; j4< m_recoJets.size(); j4++){ // j2
                if (j4 == i) continue;
                TLorentzVector J4 = m_recoJets[j4];                   
                
                for (unsigned int k = 0; k < m_recoJets.size(); k++){ // BL
                    if (k == j4) continue;
                    if (k == j3) continue;
                    if (k == i) continue;
                    TLorentzVector BL = m_recoJets[k];
                    
                    BBtagged = isBTagged[i]+isBTagged[k];
                    LBtagged = isBTagged[j3]+isBTagged[j4];

                    // killing the combination if the only btag is not on BH or BL
                    if (JBtagged == 1) {
                        if ((LBtagged ==1) && (BBtagged == 0)) continue;
                    }
                    // killing the combination if the 2 btags are not on BH and BL
                    if (JBtagged == 2) {
                        if (BBtagged != 2) continue;
                    }
                    //killing the combination if at least 2 btags are not on BH and BL
                    if (JBtagged > 2) {
                        if (BBtagged != 2) continue;
                    }
                    // <-- btag selection only if btag info is given (isBTagged/JBtagged is != 0)
                    
                    n_combi++;                      

                    TLorentzVector TMET;
                    TMET.SetPx(MEx);
                    TMET.SetPy(MEy);
                    TMET.SetPz(0);
                    TMET.SetE(0);
                    
                    // calling KinFit reconstruction (all inputs are converted here in GeV)
                    myAlienKinFit->ReadObjects(J3*(1.0/1000.),J4*(1.0/1000.),BH*(1.0/1000.),TL*(1.0/1000.),TMET*(1.0/1000.),BL*(1.0/1000.));
                    Int_t State = 0;
                    myAlienKinFit->Fit(State);
                    
                    double kf_comb = 9999999;
                    if(State == 1){
                        kf_comb = myAlienKinFit->GetKFChi2();
                    } else {
                        // Minuit didn't converge
                        kf_comb=9999999;
                    }                    
                    if (kf_comb < Best_comb) { // filling jets position for current best combination (lowest kf_comb)
                        Best_comb = kf_comb;                              
                        Sel_j1 = k;
                        Sel_bh = i;
                        Sel_j3 = j3;
                        Sel_j4 = j4;
                        Sel_lep = l;
                        Sel_bfakelep=1;
                        
                    }                                             
                }
            }
        }
    }
    }

    // if jets are selected
    if (Sel_j1+Sel_bh+Sel_j3+Sel_j4 > 0) {   
        res.push_back(Sel_j3);
        res.push_back(Sel_j4);
        res.push_back(Sel_bh);               
        res.push_back(Sel_j1);
        res.push_back(Sel_lep);
        res.push_back(Sel_bfakelep);

        //X2
        fs = log(Best_comb);        
    } else {
        fs=17;
    }
    
    return res;
}

