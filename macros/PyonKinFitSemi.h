#ifndef PYONKINFITSEMI_H
#define PYONKINFITSEMI_H

// ROOT includes
#include "TLorentzVector.h"
#include <TMinuit.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <string>
// root
#include <TMultiGraph.h>

int RecoLeptSideForPyon(TLorentzVector TheMuon, TLorentzVector vMET, TLorentzVector bJet, TLorentzVector* Neutrino, double mtop_had);
double PzPtNeutrinoForPyon ( double Px_l, double Py_l, double Pz_l, double Px_nu, double Py_nu, double *Pz, double *Pt);
double PzNeutrinoForPyon ( double Px_l, double Py_l, double Pz_l, double Px_nu, double Py_nu, double *Pz1, double *Pz2);

// modif BDE, reduction nombre de parametre car mtop fixee
static const int ParamNber = 16;
static const int ParamNber8 = 8;
static const int ParamNberFH = 6;
// const masses
static const double PDGWMass = 80.4;
static const double PDGBMass = 5;
static const double PDGTOPMass = 170;

static const double PDGMW = 80.38;
static const double FITMW = 80.38;
static const double PDGMTOP = 172;
static const double FITMTOP = 172;

static const unsigned int MaxEtaBins=4;

class PyonKinFitSemi
{ 
public:
    PyonKinFitSemi(TString ParamsFile);
    virtual ~PyonKinFitSemi();
    void SetDecayType(unsigned int Decay);
    unsigned int GetDecayType();

    void Chi2(const int &npar, double *gin, double &f, double *par, int iflag);
    void Chi2(const int &npar, double &f, double *par, int iflag);

//    void Chi2FH(const int &npar, double *gin, double &f, double *par, int iflag);
//    void Chi2FH(const int &npar, double &f, double *par, int iflag);
    
    void printConstTerms();
    // -- 
    void ReadObjects(TLorentzVector Jet1, TLorentzVector Jet2, TLorentzVector BJetH, TLorentzVector Muon, TLorentzVector Neutrino, TLorentzVector BJetL);
    void Fit(int &status);
    void Fit8(int &status);
    void FitFH(int &status);

    
    // --
    double GetKFChi2();
    const int GetParamNber();
    double GetFitVar(const int Num);
    double GetErrorFitVar(const int Num);
    inline void SetDebugMode(int DBL){ DEBUG_Level = DBL; return; }
    inline void SetOffsetMode(bool ApplyOffset){ OffsetObs = ApplyOffset ; return; }
    unsigned int ReadAlienErrors(TString ParamsFile);
    double DKFJetResol(TLorentzVector BJetL,unsigned int JetFlavor, unsigned int IPar);
    double DKFJetBias(TLorentzVector BJetL,unsigned int JetFlavor, unsigned int IPar);

    TLorentzVector GetFittedJet(unsigned int iJet, unsigned int flavor);
    TLorentzVector GetFittedMuon();
    TLorentzVector GetFittedNeutrino();

    unsigned int DecayType; //0: semileptonic, 1: fully hadronic
    
private:

  

    
    TMinuit *MyMinuit;
    TMinuit *MyMinuitFH;
    
    // const measured termes : input in Fit
    double KFChi2;
    bool   TurnJets; // allow jet angles to be changed
    bool   OffsetObs; // apply a bias correct independant from measurement correction extracted from .dat file
//
    bool UseMassEq;
    
    double MeasEtaJ1;
    double MeasEtaJ2;
    double MeasEtaBH;
    double MeasEtaBL;
    double MeasPhiJ1;
    double MeasPhiJ2;
    double MeasPhiBH;
    double MeasPhiBL;
    double MeasEJ1;
    double MeasEJ2;
    double MeasEBH;
    double MeasEBL;
    double MeasEMU;
    double MeasPxNu;
    double MeasPyNu;
    double MeasPzNu;
    double MeanMeasMTop;
    // for FH only
    double MeasEJ3;
    double MeasEJ4;
    double MeasEtaJ3;
    double MeasEtaJ4;
    double MeasPhiJ3;
    double MeasPhiJ4;
    
    // Computed termes
    double MjjViaFit;
    double MlvViaFit;
    double MjjbViaFit;
    double MlvbViaFit;
    // Other Needed 
    double MeasEtaMU;
    double MeasPhiMU;
    double FitPtSystem;
    
    double FittedParam[ParamNber];
    double ErrFitParam[ParamNber];
    // Sigma terms
    double SigmEtaJet1;
    double SigmEtaJet2;
    double SigmEtaBJetH;
    double SigmEtaBJetL;
    double SigmPhiJet1;
    double SigmPhiJet2;
    double SigmPhiBJetH;
    double SigmPhiBJetL;
    double SigmEJet1;
    double SigmEJet2;
    double SigmEBJetH;
    double SigmEBJetL;
    double SigmEMu;
    double SigmPxNu;
    double SigmPyNu;
    double SigmPzNu;
    double SigmMW;
    double SigmMT;
    double SigmPTS;
    // for FH only
    double SigmEJet3;
    double SigmEJet4;

    // Bias terms
    double BiasEtaJet1;
    double BiasEtaJet2;
    double BiasEtaBJetH;
    double BiasEtaBJetL;
    double BiasPhiJet1;
    double BiasPhiJet2;
    double BiasPhiBJetH;
    double BiasPhiBJetL;
    double BiasEJet1;
    double BiasEJet2;
    double BiasEBJetH;
    double BiasEBJetL;
    double BiasEMu;
    double BiasPxNu;
    double BiasPyNu;
    double BiasPzNu;
    double BiasMW;
    double BiasMT;
    double BiasPTS;
    // for FH only
    double BiasEJet3;
    double BiasEJet4;

    int DEBUG_Level;
    
    double AlienErrors[3][3][4][4]; // this vector contains a set of parameters

    double AlienBiases[3][3][4][4]; // this vector contains a set of parameters
    
    // used to parametrise the errors. It is filled by ReadAlienErrors() which should
    // be called once in the constructor
    
    // studied flavors
    // only two flavors are considered : Wjets=1, bjets=3
    unsigned int NStudiedFlavor;
    unsigned int StudiedFlavor[2];
//     StudiedFlavor[0]=1;
//     StudiedFlavor[1]=3;

//    double MeasParams[30];
    TLorentzVector MeasuredMuon;
    TLorentzVector MeasuredNeutrino;
    TLorentzVector MeasuredBJetH;
    TLorentzVector MeasuredBJetL;
    TLorentzVector MeasuredJet1;
    TLorentzVector MeasuredJet2;
};


#endif

