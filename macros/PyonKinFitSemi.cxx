/* **********************************************************************
 *                                                                      *
 * PyonKinFit :   kinematical fit for semilept tt bar events             *
 *                 based on
 *               - an external  full jet parametrisation                *
 *               - chi2 minisation                                      *
 *               - computed neutrino Pz (for initialisation)            *
 *               - 2 mtop + mw constraint                               *
 * Diane  CINCA        Diane.Cinca@cern.ch
 * Djamel BOUMEDIENE   Djamel.Boumedien@cern.ch                         *
 *
 *
 * TODO : parametrization of Neutrino + Muon + electron from .dat file
// internal tracking :
 ************************************************************************/

#include <TROOT.h>
#include <TMath.h>
#include <TMinuit.h>
#include "macros/PyonKinFitSemi.h"




  
double E(double M, double PX, double PY, double PZ);
double Mass(TLorentzVector V1, TLorentzVector V2);
double Mass(TLorentzVector V1, TLorentzVector V2, TLorentzVector V3);
double P(double E, double M);

static void * vdummy = 0;

// function chi2 dont le pointeur est fourni a minuit pour minimisation
static void FuncChi2(int &npar, double *ddummy, double &cf, double *par, int iflag){
  PyonKinFitSemi * mySelf = (PyonKinFitSemi*) vdummy;

  mySelf->Chi2( npar, cf, par, iflag );

}

PyonKinFitSemi::PyonKinFitSemi(TString ParamsFile) : KFChi2 ( 10.E10 ), DEBUG_Level (1), TurnJets (true), UseMassEq (false)
{ // argument = parametrisation file
    // initialisations : KFChi2 kin fit chi2 set to 10^10
    // debug level 1 by default

    
  if ( vdummy != 0 ) vdummy = 0;
  DecayType = 0; //Semilep default
  MyMinuit = new TMinuit(ParamNber);
  MyMinuitFH = new TMinuit(ParamNberFH);
  double arglist[10];
  int ierflg = 0;
  arglist[0] = 0;
  MyMinuit->mnexcm("SET STR",arglist,1,ierflg);
  MyMinuitFH->mnexcm("SET STR",arglist,1,ierflg);

  NStudiedFlavor=2;
  StudiedFlavor[0]=1;
  StudiedFlavor[1]=3;
  ReadAlienErrors(ParamsFile); // reads the errors from the .dat file => create one object PyonKinFitSemi for each type of fit (typically 1 time in your main program)
  SetOffsetMode(true);
}


PyonKinFitSemi::~PyonKinFitSemi(){
  vdummy = 0;
}



void PyonKinFitSemi::Chi2(const int &npar, double &f, double *par, int iflag){
  /*------------------------------------------------------------------------- */


    double par0;
    double par1;
    double par2;
    double par3;
    double par4;
    double par5;
    double par6;
    double par7;
    double par8;
    double par9;
    double par10;
    double par11;
    double par12;
    double par13;
    double par14;
    double par15;

    if (DecayType==0){ // Semilept
        if (!TurnJets) { // angles must be fixed to measured


            par0 =MeasEtaJ1;
            par1 =MeasEtaJ2;
            par2 =MeasEtaBH;
            par3 =MeasEtaBL;
            par4 =MeasPhiJ1;
            par5 =MeasPhiJ2;
            par6 =MeasPhiBH;
            par7 =MeasPhiBL;
            
            par8 = par[0];
            par9 = par[1];
            par10= par[2];
            par11= par[3];
            par12= par[4];
            par13= par[5];
            par14= par[6];
            par15= par[7];
            
            
        } else {
            
            par0 = par[0];
            par1 = par[1];
            par2= par[2];
            par3= par[3];
            par4= par[4];
            par5= par[5];
            par6= par[6];
            par7= par[7];
            par8 = par[8];
            par9 = par[9];
            par10= par[10];
            par11= par[11];
            par12= par[12];
            par13= par[13];
            par14= par[14];
            par15= par[15];
            
        }
        
        TLorentzVector FitJ1(par8*TMath::Cos(par4)/TMath::CosH(par0),
                             par8*TMath::Sin(par4)/TMath::CosH(par0),
                             par8*TMath::TanH(par0),par8);
        TLorentzVector FitJ2(par9*TMath::Cos(par5)/TMath::CosH(par1),
                             par9*TMath::Sin(par5)/TMath::CosH(par1),
                             par9*TMath::TanH(par1),par9);
        TLorentzVector FitBH(P(par10,PDGBMass)*TMath::Cos(par6)/TMath::CosH(par2),
                             P(par10,PDGBMass)*TMath::Sin(par6)/TMath::CosH(par2),
                             P(par10,PDGBMass)*TMath::TanH(par2),par10);
        TLorentzVector FitBL(P(par11,PDGBMass)*TMath::Cos(par7)/TMath::CosH(par3),
                             P(par11,PDGBMass)*TMath::Sin(par7)/TMath::CosH(par3),
                             P(par11,PDGBMass)*TMath::TanH(par3),par11);
        TLorentzVector FitMU(par12*TMath::Cos(MeasPhiMU)/TMath::CosH(MeasEtaMU),
                             par12*TMath::Sin(MeasPhiMU)/TMath::CosH(MeasEtaMU),
                             par12*TMath::TanH(MeasEtaMU),par12);
        TLorentzVector FitNU(par13,par14,par15,E(0,par13,par14,par15));
        
        // Compute Masses
        MjjViaFit=Mass(FitJ1,FitJ2);
        MlvViaFit=Mass(FitMU,FitNU);
        MjjbViaFit=Mass(FitJ1,FitJ2,FitBH);
        MlvbViaFit=Mass(FitMU,FitNU,FitBL);
        FitPtSystem = (FitBL+FitBH+FitMU+FitNU+FitJ1+FitJ2).Pt();
        
        // Prepare each term of Chi2
        const int TermNber=20;
        double term[TermNber];
        //std::cout << "#################PDGWMass="<<PDGWMass << "\tPDGBMass=" <<PDGBMass << std::endl;
        term[0]=TMath::Power((MeasEtaJ1-BiasEtaJet1-par0)/SigmEtaJet1,2.);
        term[1]=TMath::Power((MeasEtaJ2-BiasEtaJet2-par1)/SigmEtaJet2,2.);
        term[2]=TMath::Power((MeasEtaBH-BiasEtaBJetH-par2)/SigmEtaBJetH,2.);
        term[3]=TMath::Power((MeasEtaBL-BiasEtaBJetL-par3)/SigmEtaBJetL,2.);
        term[4]=TMath::Power((MeasPhiJ1-BiasPhiJet1-par4)/SigmPhiJet1,2.);
        term[5]=TMath::Power((MeasPhiJ2-BiasPhiJet2-par5)/SigmPhiJet2,2.);
        term[6]=TMath::Power((MeasPhiBH-BiasPhiBJetH-par6)/SigmPhiBJetH,2.);
        term[7]=TMath::Power((MeasPhiBL-BiasPhiBJetL-par7)/SigmPhiBJetL,2.);
        term[8]=TMath::Power((MeasEJ1-BiasEJet1-par8)/SigmEJet1,2.);
        term[9]=TMath::Power((MeasEJ2-BiasEJet2-par9)/SigmEJet2,2.);
        term[10]=TMath::Power((MeasEBH-BiasEBJetH-par10)/SigmEBJetH,2.);
        term[11]=TMath::Power((MeasEBL-BiasEBJetL-par11)/SigmEBJetL,2.);
        term[12]=TMath::Power((MeasEMU-BiasEMu-par12)/SigmEMu,2.);
        term[13]=TMath::Power((MeasPxNu-BiasPxNu-par13)/SigmPxNu,2.);
        term[14]=TMath::Power((MeasPyNu-BiasPyNu-par14)/SigmPyNu,2.);
        term[15]=TMath::Power((MeasPzNu-BiasPzNu-par15)/SigmPzNu,2.);
        term[16]=TMath::Power((MjjViaFit-PDGWMass)/SigmMW,2.);
        term[17]=TMath::Power((MlvViaFit-PDGWMass)/SigmMW,2.);
        
        if (UseMassEq) { // mass equality
            term[18]=TMath::Power((MjjbViaFit-MlvbViaFit)/SigmMT,2.);
            term[19]=0;
        } else { // absolute mass constraint
            term[18]=TMath::Power((MjjbViaFit-PDGTOPMass)/SigmMT,2.);
            term[19]=TMath::Power((MlvbViaFit-PDGTOPMass)/SigmMT,2.);
        }
        term[20]=TMath::Power((FitPtSystem)/SigmPTS,2.);
        
        if (TurnJets) { // fitting Eta & Phi of the jets
            
            // Compute Chi2
            f=0;
            for(int i=0; i<TermNber; ++i ){
                f+=term[i];
            }
            
        } else { // keep jet angles unchanged
            
            // Compute Chi2
            f=0;
            for(int i=8; i<TermNber; ++i ){
                f+=term[i];
            }
            
        }
        
    } else if (DecayType==1) { // FH


        par0 =MeasEtaJ1;
        par1 =MeasEtaJ2;
        par2 =MeasEtaBH;
        par3 =MeasEtaBL;
        par4 =MeasPhiJ1;
        par5 =MeasPhiJ2;
        par6 =MeasPhiBH;
        par7 =MeasPhiBL;
        
        par8 = par[0];
        par9 = par[1]; 
        par10= par[2];
        par11= par[3];
        par12= par[4];
        par13= par[5];
//        par14= par[6];
//        par15= par[7];


        
        TLorentzVector FitJ1(par8*TMath::Cos(par4)/TMath::CosH(par0),
                             par8*TMath::Sin(par4)/TMath::CosH(par0),
                             par8*TMath::TanH(par0),par8);
        TLorentzVector FitJ2(par9*TMath::Cos(par5)/TMath::CosH(par1),
                             par9*TMath::Sin(par5)/TMath::CosH(par1),
                             par9*TMath::TanH(par1),par9);
        TLorentzVector FitBH(P(par10,PDGBMass)*TMath::Cos(par6)/TMath::CosH(par2),
                             P(par10,PDGBMass)*TMath::Sin(par6)/TMath::CosH(par2),
                             P(par10,PDGBMass)*TMath::TanH(par2),par10);
        TLorentzVector FitBL(P(par11,PDGBMass)*TMath::Cos(par7)/TMath::CosH(par3),
                             P(par11,PDGBMass)*TMath::Sin(par7)/TMath::CosH(par3),
                             P(par11,PDGBMass)*TMath::TanH(par3),par11);
        TLorentzVector FitJ3(par12*TMath::Cos(MeasPhiJ3)/TMath::CosH(MeasEtaJ3),
                             par12*TMath::Sin(MeasPhiJ3)/TMath::CosH(MeasEtaJ3),
                             par12*TMath::TanH(MeasEtaJ3),par12);
        TLorentzVector FitJ4(par13*TMath::Cos(MeasPhiJ4)/TMath::CosH(MeasEtaJ4),
                             par13*TMath::Sin(MeasPhiJ4)/TMath::CosH(MeasEtaJ4),
                             par13*TMath::TanH(MeasEtaJ4),par13);

//        std::cout << "MeasEJ1: " << MeasEJ1 << " MeasEJ2: " << MeasEJ2 << " MeasEBH: " << MeasEBH << " MeasEJ3: " << MeasEJ3 << " MeasEJ4: " << MeasEJ4 << " MeasEBL: " << MeasEBL <<std::endl;
        
        // Compute Masses
        MjjViaFit=0; MlvViaFit=0; MjjbViaFit=0; MlvbViaFit=0; 
        if (MeasEJ1 != 0 && MeasEJ2 != 0) MjjViaFit=Mass(FitJ1,FitJ2);
        if (MeasEJ3 != 0 && MeasEJ4 != 0) MlvViaFit=Mass(FitJ3,FitJ4);
        if (MeasEJ1 != 0 && MeasEJ2 != 0 && MeasEBH != 0) MjjbViaFit=Mass(FitJ1,FitJ2,FitBH);
        if (MeasEJ3 != 0 && MeasEJ4 != 0 && MeasEBL != 0) MlvbViaFit=Mass(FitJ3,FitJ4,FitBL);
        FitPtSystem = (FitBL+FitBH+FitJ3+FitJ4+FitJ1+FitJ2).Pt();
        
        // Prepare each term of Chi2
        const int TermNber=18;
        double term[TermNber];
        //std::cout << "#################PDGWMass="<<PDGWMass << "\tPDGBMass=" <<PDGBMass << std::endl;
        term[0]=0;
        term[1]=0;
        term[2]=0;
        term[3]=0;
        term[4]=0;
        term[5]=0;
        term[6]=0;
        term[7]=0;
        term[8]=0;
        term[9]=0;
        term[10]=0;
        term[11]=0;
        term[12]=0;
        term[13]=0;
        term[14]=0;
        term[15]=0;
        if (MeasEJ1 != 0) term[8]=TMath::Power((MeasEJ1-BiasEJet1-par8)/SigmEJet1,2.);
        if (MeasEJ2 != 0) term[9]=TMath::Power((MeasEJ2-BiasEJet2-par9)/SigmEJet2,2.);
        if (MeasEBH != 0) term[10]=TMath::Power((MeasEBH-BiasEBJetH-par10)/SigmEBJetH,2.);
        if (MeasEBL != 0) term[11]=TMath::Power((MeasEBL-BiasEBJetL-par11)/SigmEBJetL,2.);
        if (MeasEJ3 != 0) term[12]=TMath::Power((MeasEJ3-BiasEJet3-par12)/SigmEJet3,2.);
        if (MeasEJ4 != 0) term[13]=TMath::Power((MeasEJ4-BiasEJet4-par13)/SigmEJet4,2.);
        if (MeasEJ1 != 0 && MeasEJ2 != 0) term[14]=TMath::Power((MjjViaFit-PDGWMass)/SigmMW,2.);
        if (MeasEJ3 != 0 && MeasEJ4 != 0) term[15]=TMath::Power((MlvViaFit-PDGWMass)/SigmMW,2.);
        
        if (UseMassEq) { // mass equality
            term[16]=0; term[17]=0;
            if (MeasEJ1 != 0 && MeasEJ2 != 0 && MeasEBH != 0) term[16]=TMath::Power((MjjbViaFit-MlvbViaFit)/SigmMT,2.);
        } else { // absolute mass constraint
            term[16]=0; term[17]=0;
            if (MeasEJ1 != 0 && MeasEJ2 != 0 && MeasEBH != 0) term[16]=TMath::Power((MjjbViaFit-PDGTOPMass)/SigmMT,2.);
            if (MeasEJ3 != 0 && MeasEJ4 != 0 && MeasEBL != 0) term[17]=TMath::Power((MlvbViaFit-PDGTOPMass)/SigmMT,2.);
        }
        term[18]=TMath::Power((FitPtSystem)/SigmPTS,2.);

        // for (unsigned int toto=8; toto<19; ++toto) std:: cout << "term[" << toto << "] :" << term[toto];
        // std::cout << std::endl;

        
        // Compute Chi2
        f=0;
        for(int i=8; i<TermNber; ++i ){
            f+=term[i];
        }
        
        
    }

  
}


  

void PyonKinFitSemi::printConstTerms(){
  std::cout<<"MeasEtaJ1="<<MeasEtaJ1<<"\tSigmEtaJet1="<<SigmEtaJet1<<"\tMeasPhiJ1="<<MeasPhiJ1<<"\tSigmPhiJet1="<<SigmPhiJet1<<"\tMeasEJ1="<<MeasEJ1<<"\tSigmEJet1="<<SigmEJet1<<std::endl;
  std::cout<<"MeasEtaJ2="<<MeasEtaJ2<<"\tSigmEtaJet2="<<SigmEtaJet2<<"\tMeasPhiJ2="<<MeasPhiJ2<<"\tSigmPhiJet2="<<SigmPhiJet2<<"\tMeasEJ2="<<MeasEJ2<<"\tSigmEJet2="<<SigmEJet2<<std::endl;
  std::cout<<"MeasEtaBH="<<MeasEtaBH<<"\tSigmEtaBJetH="<<SigmEtaBJetH<<"\tMeasPhiBH="<<MeasPhiBH<<"\tSigmPhiBJetH="<<SigmPhiBJetH<<"\tMeasEBH="<<MeasEBH<<"\tSigmEBJetH="<<SigmEBJetH<<std::endl;
  std::cout<<"MeasEMU="<<MeasEMU<<"\tSigmEMu="<<SigmEMu<<std::endl;
  std::cout<<"MeasPxNu="<<MeasPxNu<<"\tSigmPxNu="<<SigmPxNu<<"\tMeasPyNu="<<MeasPyNu<<"\tSigmPyNu="<<SigmPyNu<<"\tMeasPzNu="<<MeasPzNu<<"\tSigmPzNu="<<SigmPzNu<<std::endl;
  std::cout<<"MeasEtaBL="<<MeasEtaBL<<"\tSigmEtaBJetL="<<SigmEtaBJetL<<"\tMeasPhiBL="<<MeasPhiBL<<"\tSigmPhiBJetH="<<SigmPhiBJetH<<"\tMeasEBL="<<MeasEBL<<"\tSigmEBJetH="<<SigmEBJetH<<std::endl;
  std::cout<<"SigmMW="<<SigmMW<<"\tSigmMT="<<SigmMT<<std::endl<<std::endl;
}


unsigned int PyonKinFitSemi::ReadAlienErrors(TString ParamsFile){

    // shouble be called once to read the errors from
    // the data file
    // output err is double[3][3][4][4] type
    unsigned int res=0;

    if (DEBUG_Level>=0) std::cout<<"\n========================================================\n Reading parametrisations for PyonKinFitSemi from "<<ParamsFile<<"\n--------------------------------------------------------"<<std::endl;
    
    std::ifstream ofile(ParamsFile);
    if (!ofile) {
        std::cout<<" <!> ERROR When opening parametrisation file in PyonKinFitSemi ("<<ParamsFile<<")"<<std::endl;
        return 1; // file not found
    }
    std::string var="%%";
    
    int ieta=0, ivar=0, iflavor=0; 
    double A=0, B=0, C=0, D=0;

    unsigned int security=0;
    while(var!="%%Parametrisation_starts_here%%"){
        ofile>>var;
        security++;
        if (security>500) {
            std::cout<<" <!> ERROR : Parametrisation file is empty or corrupted .. or you have more then 500 lines of comments !"<<std::endl;
            return 5;
        }
    }
    unsigned int ndata=0,nz=0;
    
    while (ndata<48){ // -- 48 depuis ajout biais, 24 avec resol seule
        
        ofile>>ivar>>ieta>>iflavor>>A>>B>>C>>D;
        ndata++;
        
        bool ivfound=false;
        for (unsigned int iv=0; iv<NStudiedFlavor; iv++) {
            if (StudiedFlavor[iv]==iflavor) {
                iflavor=iv;
                ivfound=true;
            }
        }
        if (!ivfound) {
            std::cout<<" <!> Warning : unexpected flavour ("<<iflavor<<") in params file"<<ParamsFile<<"... line ignored !\n "<<NStudiedFlavor<<" expected flavors : ";
            for (unsigned int iv=0; iv<NStudiedFlavor; iv++) std::cout<<" "<<StudiedFlavor[iv];
            std::cout<<". Parametrisation file is probably inadapted."<<std::endl;
            res=2;
            continue;
        }
        if (ieta>MaxEtaBins) {
            std::cout<<" <!> Warning : unknown Eta Bin "<<ieta<<" : line ignored ! Parametrisation file is probably inadapted."<<std::endl;
            res=3;
            continue;
        }



        if (ndata%2 == 1) {
            AlienErrors[ivar][iflavor][ieta][0]=A;
            AlienErrors[ivar][iflavor][ieta][1]=B;
            AlienErrors[ivar][iflavor][ieta][2]=C;
            AlienErrors[ivar][iflavor][ieta][3]=D;
            if ((A==0)&&(B==0)&&(C==0)&&(D==0)) nz++;

            if (DEBUG_Level>=0) {
                std::cout<<" SigmaVar "<<ivar<<", Flavor "<<iflavor<<", EtaBin "<<ieta<<" | params = ";
                std::cout<<A<<" "<<B<<" "<<C<<" "<<D<<std::endl;
            }
            
        } else {
            AlienBiases[ivar][iflavor][ieta][0]=A;
            AlienBiases[ivar][iflavor][ieta][1]=B;
            AlienBiases[ivar][iflavor][ieta][2]=C;
            AlienBiases[ivar][iflavor][ieta][3]=D;

            if (DEBUG_Level>=0) {
                std::cout<<" BiasVar "<<ivar<<", Flavor "<<iflavor<<", EtaBin "<<ieta<<" | params = ";
                std::cout<<A<<" "<<B<<" "<<C<<" "<<D<<std::endl;
            }
            
        }
        

        
    }

    if (nz != 0) {
        std::cout<<" <!> WARNING \n Empty lines or all constants set to zero for "<<nz<<" parameters in "<<ParamsFile<<"... \n"<<std::endl;
        res=3;
    }
    if (DEBUG_Level>=0) {
        if (res==0) {
            std::cout<<" OK ..."<<std::endl;
        }
    }
    if (res!=0) std::cout<<" <!> Ended with some warnings/errors PyonKinFitSemi will not work properly ... ("<<res<<") "<<std::endl;
    
    
    if (DEBUG_Level>=0) std::cout<<"======================================================="<<std::endl;
    
    ofile.close();
    return res;
}


// ------------------------------------------------------------------------------------------
void PyonKinFitSemi::ReadObjects(TLorentzVector Jet1, TLorentzVector Jet2, TLorentzVector BJetH, TLorentzVector Muon, TLorentzVector Neutrino, TLorentzVector BJetL){

    // TL order of 6 objects has to be  llb llb

    unsigned int MaxResolKind=2;
    unsigned int ResolKind=1; // 1=parametree en energie
    ResolKind=2; // Energie + Eta
    
    if (DecayType==0){ // semilept case
    
        // measured objects are stored in  MeasuredMuon, MeasuredNeutrino, MeasuredBJetH, MeasuredBJetL, MeasuredJet1, MeasuredJet2
        MeasuredMuon=Muon;
        MeasuredBJetH=BJetH;
        MeasuredBJetL=BJetL;
        MeasuredJet1=Jet1;
        MeasuredJet2=Jet2;
    
        // we compute de Pz of the neutrino
        RecoLeptSideForPyon(Muon, Neutrino, BJetL, &MeasuredNeutrino,  (MeasuredBJetH+Jet1+Jet2).M()  );
        
        MeasPzNu=MeasuredNeutrino.Pz();
        
        MeasEtaJ1=Jet1.Eta();
        MeasEtaJ2=Jet2.Eta();
        MeasEtaBH=BJetH.Eta();
        MeasEtaBL=BJetL.Eta();
        MeasPhiJ1=Jet1.Phi();
        MeasPhiJ2=Jet2.Phi();
        MeasPhiBH=BJetH.Phi();
        MeasPhiBL=BJetL.Phi();
        MeasEJ1=Jet1.E();
        MeasEJ2=Jet2.E();
        MeasEBH=BJetH.E();
        MeasEBL=BJetL.E();
        
        MeasEMU=Muon.E();
        
        MeasEtaMU=Muon.Eta();
        MeasPhiMU=Muon.Phi();
        MeasPxNu=Neutrino.Px();
        MeasPyNu=Neutrino.Py();

        

        if (ResolKind==1) {  // -- resolutions hardcodees - peut servir si on a paume le kfparams.dat
        
            // resol en energie
            SigmEJet1=6.567+0.6551*sqrt(Jet1.E());
            SigmEJet2=6.567+0.6551*sqrt(Jet2.E());
            SigmEBJetH=8.301+0.7148*sqrt(BJetH.E());
            SigmEBJetL=8.301+0.7148*sqrt(BJetL.E());
            
            //resol en eta
            SigmEtaJet1=0.00329+(0.1/TMath::Sqrt(Jet1.E()))+(1.928/Jet1.E());
            SigmEtaJet2=0.00329+(0.1/TMath::Sqrt(Jet2.E()))+(1.928/Jet2.E());
            SigmEtaBJetH=0.02588+(0.1369/TMath::Sqrt(BJetH.E()))+(1.92/BJetH.E());
            SigmEtaBJetL=0.02588+(0.1369/TMath::Sqrt(BJetL.E()))+(1.92/BJetL.E());
            
            //resol en phi
            SigmPhiJet1=0.01332+(0.5604/TMath::Sqrt(Jet1.E()))+(1.673/Jet1.E());
            SigmPhiJet2=0.01332+(0.5604/TMath::Sqrt(Jet2.E()))+(1.673/Jet2.E());
            SigmPhiBJetH=0.01257+(0.4647/TMath::Sqrt(BJetH.E()))+(2.429/BJetH.E());
            SigmPhiBJetL=0.01257+(0.4647/TMath::Sqrt(BJetL.E()))+(2.429/BJetL.E());
            
            //SigmEMu=0.29743+0.0146581*Muon.E();
            
            SigmEMu=-0.05832+0.02652*Muon.E();
            
            /*SigmPxNu=14.2827;
              SigmPyNu=13.9769;
              SigmPzNu=26.1467;*/
            
            std::cout<<" WARNING : Using hard coded parametrisation : No more maintained... "<<std::endl;
            
            SigmPxNu=12.63;
            SigmPyNu=13.04;
            SigmPzNu=25.01;
            
//  SigmMW=7.61678;
//  SigmMT=12.8734;
            
            SigmMW=11.;
            SigmMT=21.;
            SigmPTS=33.;        
            
            
        } else if (ResolKind==2) { // -- Utiliser cette parametrisation
            
            // resol en energie
            SigmEJet1=DKFJetResol(Jet1,1,1); // arg1 : TLorentzVector, arg2: jet type (1=W, 2=b), arg3: type de resolution 1=Ene, 2=Eta, 3=Phi        
            SigmEJet2=DKFJetResol(Jet2,1,1); // arg1 : TLorentzVector, arg2: jet type (1=W, 2=b), arg3: type de resolution 1=Ene, 2=Eta, 3=Phi
            SigmEBJetH=DKFJetResol(BJetH,2,1); // arg1 : TLorentzVector, arg2: jet type (1=W, 2=b), arg3: type de resolution 1=Ene, 2=Eta, 3=Phi
            SigmEBJetL=DKFJetResol(BJetL,2,1); // arg1 : TLorentzVector, arg2: jet type (1=W, 2=b), arg3: type de resolution 1=Ene, 2=Eta, 3=Phi
            
            
            //resol en phi
            SigmPhiJet1=DKFJetResol(Jet1,1,3);
            SigmPhiJet2=DKFJetResol(Jet2,1,3);
            SigmPhiBJetH=DKFJetResol(BJetH,2,3);
            SigmPhiBJetL=DKFJetResol(BJetL,2,3);
            
            //resol en Eta
            SigmEtaJet1=DKFJetResol(Jet1,1,2);
            SigmEtaJet2=DKFJetResol(Jet2,1,2);
            SigmEtaBJetH=DKFJetResol(BJetH,2,2);
            SigmEtaBJetL=DKFJetResol(BJetL,2,2);
            
            SigmEMu=-0.05832+0.02652*Muon.E();
            
            
            SigmPxNu=15.63;
            SigmPyNu=15.04;
            SigmPzNu=35.01;
            
            SigmMW=4.; // 7  // 4
            SigmMT=5.; // 12   //3
            SigmPTS=5.; // 33  // 5
            
            
            
            // resol en energie
            BiasEJet1=DKFJetBias(Jet1,1,1); // arg1 : TLorentzVector, arg2: jet type (1=W, 2=b), arg3: type de resolution 1=Ene, 2=Eta, 3=Phi        
            BiasEJet2=DKFJetBias(Jet2,1,1); // arg1 : TLorentzVector, arg2: jet type (1=W, 2=b), arg3: type de resolution 1=Ene, 2=Eta, 3=Phi
            BiasEBJetH=DKFJetBias(BJetH,2,1); // arg1 : TLorentzVector, arg2: jet type (1=W, 2=b), arg3: type de resolution 1=Ene, 2=Eta, 3=Phi
            BiasEBJetL=DKFJetBias(BJetL,2,1); // arg1 : TLorentzVector, arg2: jet type (1=W, 2=b), arg3: type de resolution 1=Ene, 2=Eta, 3=Phi
            
            
            //resol en phi
            BiasPhiJet1=DKFJetBias(Jet1,1,3)/5;
            BiasPhiJet2=DKFJetBias(Jet2,1,3)/5;
            BiasPhiBJetH=DKFJetBias(BJetH,2,3)/5;
            BiasPhiBJetL=DKFJetBias(BJetL,2,3)/5;
            
            //resol en Eta
            BiasEtaJet1=DKFJetBias(Jet1,1,2)/4;
            BiasEtaJet2=DKFJetBias(Jet2,1,2)/4;
            BiasEtaBJetH=DKFJetBias(BJetH,2,2)/4;
            BiasEtaBJetL=DKFJetBias(BJetL,2,2)/4;
            
            BiasEMu = 0;
            
            BiasPxNu = 0;
            BiasPyNu = 0;
            BiasPzNu = 0;
            
            
        } else {
            std::cout<<" <!> Fatal Error : You are asking for an unknown error parametrisation ..."<<std::endl;
            std::cout<<"     ResolKind = "<<ResolKind<<" : should be in the range 1.."<<MaxResolKind<<std::endl;
        }
        
    } else if (DecayType==1){ // FH case


        // measured objects are stored in  MeasuredMuon, MeasuredNeutrino, MeasuredBJetH, MeasuredBJetL, MeasuredJet1, MeasuredJet2
        MeasuredMuon=Muon;
        MeasuredBJetH=BJetH;
        MeasuredBJetL=BJetL;
        MeasuredJet1=Jet1;
        MeasuredJet2=Jet2;
        MeasuredNeutrino=Neutrino;
        
        MeasEtaJ1=Jet1.Eta();
        MeasEtaJ2=Jet2.Eta();
        MeasEtaBH=BJetH.Eta();
        MeasEtaBL=BJetL.Eta();
        MeasPhiJ1=Jet1.Phi();
        MeasPhiJ2=Jet2.Phi();
        MeasPhiBH=BJetH.Phi();
        MeasPhiBL=BJetL.Phi();
        MeasEJ1=Jet1.E();
        MeasEJ2=Jet2.E();
        MeasEBH=BJetH.E();
        MeasEBL=BJetL.E();
        
        MeasEJ3=Muon.E();
        MeasEJ4=Neutrino.E();

        MeasEtaJ3=Muon.Eta();
        MeasEtaJ4=Neutrino.Eta();

        MeasPhiJ3=Muon.Phi();
        MeasPhiJ4=Neutrino.Phi();
        
        // resol en energie
        SigmEJet1=DKFJetResol(Jet1,1,1); // arg1 : TLorentzVector, arg2: jet type (1=W, 2=b), arg3: type de resolution 1=Ene, 2=Eta, 3=Phi        
        SigmEJet2=DKFJetResol(Jet2,1,1); // arg1 : TLorentzVector, arg2: jet type (1=W, 2=b), arg3: type de resolution 1=Ene, 2=Eta, 3=Phi
        SigmEBJetH=DKFJetResol(BJetH,2,1); // arg1 : TLorentzVector, arg2: jet type (1=W, 2=b), arg3: type de resolution 1=Ene, 2=Eta, 3=Phi
        SigmEBJetL=DKFJetResol(BJetL,2,1); // arg1 : TLorentzVector, arg2: jet type (1=W, 2=b), arg3: type de resolution 1=Ene, 2=Eta, 3=Phi
        
        SigmEJet3=DKFJetResol(Muon,1,1); // arg1 : TLorentzVector, arg2: jet type (1=W, 2=b), arg3: type de resolution 1=Ene, 2=Eta, 3=Phi        
        SigmEJet4=DKFJetResol(Neutrino,1,1); // arg1 : TLorentzVector, arg2: jet type (1=W, 2=b), arg3: type de resolution 1=Ene, 2=Eta, 3=Phi
        
        //resol en phi
        SigmPhiJet1=DKFJetResol(Jet1,1,3);
        SigmPhiJet2=DKFJetResol(Jet2,1,3);
        SigmPhiBJetH=DKFJetResol(BJetH,2,3);
        SigmPhiBJetL=DKFJetResol(BJetL,2,3);
        
        //resol en Eta
        SigmEtaJet1=DKFJetResol(Jet1,1,2);
        SigmEtaJet2=DKFJetResol(Jet2,1,2);
        SigmEtaBJetH=DKFJetResol(BJetH,2,2);
        SigmEtaBJetL=DKFJetResol(BJetL,2,2);
                
        SigmMW=4.; // 7  // 4
        SigmMT=5.; // 12   //3
        SigmPTS=5.; // 33  // 5
        
                
        // resol en energie
        BiasEJet1=DKFJetBias(Jet1,1,1); // arg1 : TLorentzVector, arg2: jet type (1=W, 2=b), arg3: type de resolution 1=Ene, 2=Eta, 3=Phi        
        BiasEJet2=DKFJetBias(Jet2,1,1); // arg1 : TLorentzVector, arg2: jet type (1=W, 2=b), arg3: type de resolution 1=Ene, 2=Eta, 3=Phi
        BiasEBJetH=DKFJetBias(BJetH,2,1); // arg1 : TLorentzVector, arg2: jet type (1=W, 2=b), arg3: type de resolution 1=Ene, 2=Eta, 3=Phi
        BiasEBJetL=DKFJetBias(BJetL,2,1); // arg1 : TLorentzVector, arg2: jet type (1=W, 2=b), arg3: type de resolution 1=Ene, 2=Eta, 3=Phi
        
        BiasEJet3=DKFJetBias(Muon,1,1); // arg1 : TLorentzVector, arg2: jet type (1=W, 2=b), arg3: type de resolution 1=Ene, 2=Eta, 3=Phi        
        BiasEJet4=DKFJetBias(Neutrino,1,1); // arg1 : TLorentzVector, arg2: jet type (1=W, 2=b), arg3: type de resolution 1=Ene, 2=Eta, 3=Phi
        
        //resol en phi
        BiasPhiJet1=DKFJetBias(Jet1,1,3)/5;
        BiasPhiJet2=DKFJetBias(Jet2,1,3)/5;
        BiasPhiBJetH=DKFJetBias(BJetH,2,3)/5;
        BiasPhiBJetL=DKFJetBias(BJetL,2,3)/5;
        
        //resol en Eta
        BiasEtaJet1=DKFJetBias(Jet1,1,2)/4;
        BiasEtaJet2=DKFJetBias(Jet2,1,2)/4;
        BiasEtaBJetH=DKFJetBias(BJetH,2,2)/4;
        BiasEtaBJetL=DKFJetBias(BJetL,2,2)/4;
        
        BiasEMu = 0;
        
        BiasPxNu = 0;
        BiasPyNu = 0;
        BiasPzNu = 0;
        
        
        
    } else {
        std::cout<<"ERROR, unknown decay type requested: "<<DecayType<<". Objects ignored."<<std::endl;
    }
        
//    MeanMeasMTop=0.5*(Mass(Jet1,Jet2,BJetH)+Mass(Muon,Neutrino,BJetL));
  // Sigma termes
    

    
}

//--------------------------------------------------------------------

void PyonKinFitSemi::Fit(int &status){
  KFChi2=10.E10;
  if ( vdummy != 0 ){
    std::cout << "PyonKinFitSemi Error"<< vdummy
	      << std::endl;
	      }
  vdummy = (void*) this;

  int FitParNber = 16;

  MyMinuit->mnrset(1);
  MyMinuit->SetPrintLevel(DEBUG_Level); // debug level
    
  MyMinuit->SetFCN(&FuncChi2); // fonction to be minimised
  double arglist[10];
  int ierflg = 0;
  arglist[0] = 1;
  MyMinuit->mnexcm("SET ERR", arglist ,1,ierflg); 
  
  // Initialize Minuit Parameters
  int ParamNum[ParamNber];
  
  TString Name[ParamNber]={"FitEtaJ1","FitEtaJ2","FitEtaBH","FitEtaBL","FitPhiJ1","FitPhiJ2","FitPhiBH","FitPhiBL","FitEJ1","FitEJ2","FitEBH","FitEBL","FitEMU","FitPxNu","FitPyNu","FitPzNu"};

  double vstart[ParamNber] = {MeasEtaJ1,MeasEtaJ2,MeasEtaBH,MeasEtaBL,MeasPhiJ1,MeasPhiJ2,MeasPhiBH,MeasPhiBL,MeasEJ1,MeasEJ2,MeasEBH,MeasEBL,MeasEMU,MeasPxNu,MeasPyNu,MeasPzNu};

  double step[ParamNber];
  double LowerBound[ParamNber];
  double UpperBound[ParamNber];
  for(int i =0 ; i < ParamNber ; ++i ){
    ParamNum[i]=i;
    if(i <=8 ){  // step 1E-8 for eta and phi
      step[i]=1E-6;
    }else{ // step 1E-4 for 4Mom composant
      step[i]=1E-4;
    }
      LowerBound[i]=0.;
      UpperBound[i]=0.;
  }
  for(int i =0 ; i <ParamNber ; ++i ){
    MyMinuit->mnparm(ParamNum[i],Name[i], vstart[i], step[i],LowerBound[i],UpperBound[i],ierflg);
    if(ierflg){
      std::cout << "PROBLEM WITH PARAMETER " << i << "\t => ierflg=" << ierflg 
		<< std::endl;
      std::cout << "ParamNum= " << ParamNum[i] << "\tName = " << Name[i] 
		<< "\tvstart=" << vstart[i] << "\tstep="<< step[i] 
		<< std::endl;
      break;
    }
  }
  
  arglist[0] = 1000;
  arglist[1] = 1;
  // minimisation
  MyMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);
  //status = 1 if minuit output is "CONVERGED";
  status = ((MyMinuit->fCstatu == "CONVERGED ") ? 1 : 0 );

  vdummy = 0;

  // Print results
  double amin,edm,errdef;
  int nvpar,nparx,icstat;


  MyMinuit->mnstat(amin,edm,errdef,nvpar,nparx,icstat);
  
  // Get parameters
  for(int i =0 ; i <ParamNber ; ++i ){
    MyMinuit->GetParameter(i,FittedParam[i], ErrFitParam[i]);
  }

  Chi2(ParamNber,KFChi2,FittedParam , 0);

  return;
}






//--------------------------------------------------------------------

void PyonKinFitSemi::FitFH(int &status){
  KFChi2=10.E10;
  if ( vdummy != 0 ){
    std::cout << "PyonKinFitSemi Error"<< vdummy
	      << std::endl;
	      }
  vdummy = (void*) this;

  int FitParNber = 6;

  MyMinuitFH->mnrset(1);
  MyMinuitFH->SetPrintLevel(DEBUG_Level); // debug level
    
  MyMinuitFH->SetFCN(&FuncChi2); // fonction to be minimised
  double arglist[10];
  int ierflg = 0;
  arglist[0] = 1;
  MyMinuitFH->mnexcm("SET ERR", arglist ,1,ierflg); 

  // Initialize Minuit Parameters
  int ParamNum[ParamNberFH];
  
  TString Name[ParamNberFH]={"FitEJ1","FitEJ2","FitEBH","FitEBL","FitEJ3","FitEJ4"};

  double vstart[ParamNberFH] = {MeasEJ1,MeasEJ2,MeasEBH,MeasEBL,MeasEJ3,MeasEJ4};

  double step[ParamNberFH];
  double LowerBound[ParamNberFH];
  double UpperBound[ParamNberFH];
  for(int i =0 ; i < ParamNberFH ; ++i ){
    ParamNum[i]=i;
//    step[i]=1E-6;
    step[i]=1E-4;
    
    LowerBound[i]=0.;
    UpperBound[i]=0.;
  }
  for(int i =0 ; i <ParamNberFH ; ++i ){
    MyMinuitFH->mnparm(ParamNum[i],Name[i], vstart[i], step[i],LowerBound[i],UpperBound[i],ierflg);
    if(ierflg){
      std::cout << "PROBLEM WITH PARAMETER " << i << "\t => ierflg=" << ierflg 
		<< std::endl;
      std::cout << "ParamNum= " << ParamNum[i] << "\tName = " << Name[i] 
		<< "\tvstart=" << vstart[i] << "\tstep="<< step[i] 
		<< std::endl;
      break;
    }
  }
  
  arglist[0] = 1000;
  arglist[1] = 1;
  // minimisation
  MyMinuitFH->mnexcm("MIGRAD", arglist ,2,ierflg);
  //status = 1 if minuit output is "CONVERGED";
  status = ((MyMinuitFH->fCstatu == "CONVERGED ") ? 1 : 0 );

  vdummy = 0;

  // Print results
  double amin,edm,errdef;
  int nvpar,nparx,icstat;


  MyMinuitFH->mnstat(amin,edm,errdef,nvpar,nparx,icstat);
  
  // Get parameters
  for(int i =0 ; i <ParamNberFH ; ++i ){
      MyMinuitFH->GetParameter(i,FittedParam[i], ErrFitParam[i]);
  }
  
  Chi2(ParamNberFH,KFChi2,FittedParam , 0);

  return;
}






//----------------------------------------------

void PyonKinFitSemi::Fit8(int &status){
  KFChi2=10.E10;
  if ( vdummy != 0 ){
    std::cout << "PyonKinFitSemi Error"<< vdummy
	      << std::endl;
	      }
  vdummy = (void*) this;

  int FitParNber = 8;

  TurnJets = false;
  
  MyMinuit->mnrset(1);
  MyMinuit->SetPrintLevel(DEBUG_Level); // debug level
//  MyMinuit->SetPrintLevel(-1); // debug level
    
  MyMinuit->SetFCN(&FuncChi2); // fonction to be minimised
  double arglist[10];
  int ierflg = 0;
  arglist[0] = 1;
  MyMinuit->mnexcm("SET ERR", arglist ,1,ierflg); 

  // Initialize Minuit Parameters
  int ParamNum[ParamNber8];
  
  TString Name[ParamNber8]={"FitEJ1","FitEJ2","FitEBH","FitEBL","FitEMU","FitPxNu","FitPyNu","FitPzNu"};

  double vstart[ParamNber8] = {MeasEJ1,MeasEJ2,MeasEBH,MeasEBL,MeasEMU,MeasPxNu,MeasPyNu,MeasPzNu};

  double step[ParamNber8];
  double LowerBound[ParamNber8];
  double UpperBound[ParamNber8];
  for(int i =0 ; i < ParamNber8 ; ++i ){
    ParamNum[i]=i;

    step[i]=1E-4;

    LowerBound[i]=0.;
    UpperBound[i]=0.;
  }
  for(int i =0 ; i <ParamNber8 ; ++i ){
    MyMinuit->mnparm(ParamNum[i],Name[i], vstart[i], step[i],LowerBound[i],UpperBound[i],ierflg);
    if(ierflg){
      std::cout << "PROBLEM WITH PARAMETER " << i << "\t => ierflg=" << ierflg 
		<< std::endl;
      std::cout << "ParamNum= " << ParamNum[i] << "\tName = " << Name[i] 
		<< "\tvstart=" << vstart[i] << "\tstep="<< step[i] 
		<< std::endl;
      break;
    }
  }
  
  arglist[0] = 1000;
  arglist[1] = 1;
  // minimisation
  MyMinuit->mnexcm("MIGRAD", arglist ,2,ierflg);
  //status = 1 if minuit output is "CONVERGED";
  status = ((MyMinuit->fCstatu == "CONVERGED ") ? 1 : 0 );

  vdummy = 0;

  // Print results
  double amin,edm,errdef;
  int nvpar,nparx,icstat;


  MyMinuit->mnstat(amin,edm,errdef,nvpar,nparx,icstat);
  
  // Get parameters
  for(int i =0 ; i <ParamNber8 ; ++i ){
    MyMinuit->GetParameter(i,FittedParam[i], ErrFitParam[i]);
  }

  Chi2(ParamNber8,KFChi2,FittedParam , 0);

//  TurnJets = true;
  
  return;
}





//----------------------------------------------
void PyonKinFitSemi::SetDecayType(unsigned int Decay){
    DecayType = Decay;
}

unsigned int PyonKinFitSemi::GetDecayType(){
    return DecayType;
}

double PyonKinFitSemi::GetKFChi2(){
  return KFChi2;
}

const int PyonKinFitSemi::GetParamNber(){
  return ParamNber;
}

double PyonKinFitSemi::GetFitVar(const int Num){
  //if(sizeof(FittedParam)/sizeof(KFChi2) == sizeof((int)ParNber)) return FittedParam[num];
  //else return 0;
  return FittedParam[Num];
}

double PyonKinFitSemi::GetErrorFitVar(const int Num){
  //if(sizeof(FittedParam)/sizeof(KFChi2) == sizeof(ParNber)) return ErrFitParam[Num];
  //else return 0;
  return ErrFitParam[Num];
}

// -----------------------------------------------------------------------------------------------
// DKFJetResol provides an uncertainty for each jet
// input : TLorentzVector
//         JetFlavor : 1 or 2 (W or b)
//         IPar : parameter for which the uncertainty is computed
//              = 1, 2 or 3 (Energy, Eta or Phi)
// output : Absolute Uncertainty on the parameter IPar
// Remark : This function requires AlienErrors[][][][] to be filled
//          by the function ReadAlienErrors() called once when the
//          object PyonKinFitSemi is created
// -----------------------------------------------------------------------------------------------
double PyonKinFitSemi::DKFJetResol(TLorentzVector Jet,unsigned int JetFlavor, unsigned int IPar) {

    double res=1E10;
    
    if (IPar>3) {
        std::cout<<" <!> ERROR in DKFJetResol. Unknown parameter "<<IPar<<". Parameter number is limitted to 1 -> 3"<<std::endl;
        return 1E10;
    }

    if ((AlienErrors[0][0][0][0]==0)&&(AlienErrors[0][0][0][1]==0)&&(AlienErrors[0][0][0][2]==0)&&(AlienErrors[0][0][0][3]==0)) {
        // A=B=C=D=0 for E parametrisation (Wj) in all Eta bins
        std::cout<<" <!> ERROR in PyonKinFitSemi. Errors are not loaded ! Error set to 1E10 ..."<<std::endl;
        return 1E10;
    }
    
    double A=0, B=0, C=0, D=0; // parametres pour JetW
    double Ab=0, Bb=0, Cb=0, Db=0; // parametres pour Jetb

    double Eta=Jet.Eta();
    double Ene=Jet.E();

    unsigned int ieta=0;
    /*


        // anciennes resolutions (hard codees)
    
    if (fabs(Eta)<1.4) { // barrel
        if (IPar==1) { // Energie resol
            A=7.914; B=0.4866; C=0.; D=0;
            Ab=11.09; Bb=0.4525; C=0; D=0;
        } else if (IPar==2) { // Eta resol
            A=0.03671; B=0.; C=1.917; D=0;
            Ab=0.03358; Bb=0; C=2.01; D=0;
        } else if (IPar==3) { // Phi resol
            A=0.04292; B=0; C=3.24; D=0;
            Ab=0.03992; Bb=0; Cb=3.014; Db=22.;
        }        
    } else if ((fabs(Eta)>=1.4)&&(fabs(Eta)<1.7)) { // crak
        if (IPar==1) { // Energie resol
            A=11.12; B=0.7808; C=0.; D=0;
            Ab=9.444; Bb=1.055; C=0; D=0;
        } else if (IPar==2) { // Eta resol
            A=0.01707; B=0.; C=6.266; D=68;
            Ab=0.02094; Bb=0; C=4.546; D=82;
        } else if (IPar==3) { // Phi resol
            A=0.03061; B=0; C=7.282; D=0;
            Ab=0.05204; Bb=0; Cb=7.282; Db=0.;
        }        
    } else if ((fabs(Eta)>=1.7)&&(fabs(Eta)<2.6)) { // end cap
        if (IPar==1) { // Energie resol
            A=8.857; B=1.273; C=0.; D=0;
            Ab=14.9; Bb=0.87; C=0; D=0;
        } else if (IPar==2) { // Eta resol
            A=0.03305; B=0.; C=8.025; D=0;
            Ab=0.02157; Bb=0; C=10.3; D=0;
        } else if (IPar==3) { // Phi resol
            A=0.03825; B=0; C=10.96; D=0;
            Ab=0.01; Bb=0; Cb=15.43; Db=0.;
        }        
    } else if (fabs(Eta)>=2.6) { // tres bas angle
        if (IPar==1) { // Energie resol
            A=57.96; B=0; C=0; D=0;
            Ab=64.8; Bb=0.; C=0; D=0;
        } else if (IPar==2) { // Eta resol
            A=0.07666; B=0.; C=3.08; D=0;
            Ab=0.06305; Bb=0; C=13.11; D=0;
        } else if (IPar==3) { // Phi resol
            A=0.06782; B=0; C=9.1; D=0;
            Ab=0.06782; Bb=0; Cb=9.1; Db=0.;
        }        
    }


    */
// 1.4 1.7 2.6
    if (fabs(Eta)<0.8) { // barrel
        ieta = 0;
    } else if ((fabs(Eta)>=0.8)&&(fabs(Eta)<1.6)) { // crak
        ieta = 1;
    } else if ((fabs(Eta)>=1.6)&&(fabs(Eta)<2.6)) { // end cap
        ieta = 2;
    } else if (fabs(Eta)>=2.6) { // tres bas angle
        ieta = 3;
    }

//    std::cout<<A<<" "<<B<<" "<<C<<" "<<D<<std::endl;
    
    if (Ene>0) {
        if (JetFlavor==1) {
            res = (A+(B*sqrt(Ene))+(C/Ene)+(D/(Ene*Ene)));
        } else if (JetFlavor==2) {
            res = (Ab+(Bb*sqrt(Ene))+(Cb/Ene)+(Db/(Ene*Ene)));
        }
    } else {
        if (JetFlavor==1) {
            res = A;
        } else if (JetFlavor==2) {
             res = Ab;
        }  
     }

    // nouvelles (par fichier .dat)
    A=AlienErrors[IPar-1][JetFlavor-1][ieta][0];
    B=AlienErrors[IPar-1][JetFlavor-1][ieta][1];
    C=AlienErrors[IPar-1][JetFlavor-1][ieta][2];
    D=AlienErrors[IPar-1][JetFlavor-1][ieta][3];

//    std::cout<<" ipar = "<<IPar<<" flav "<<JetFlavor-1<<" ieta "<<ieta<<std::endl;
//    std::cout<<A<<" "<<B<<" "<<C<<" "<<D<<std::endl<<std::endl;

//    std::cout<<" res "<<res<<" -> ";
    
     if (Ene>0.1) {
//         res= (A+(B*sqrt(Ene))+(C/Ene)+(D/(Ene*Ene)));
         res= (A+(B*sqrt(Ene))-(C/sqrt(Ene)));

         if (IPar>1){ // eta et phi
             res =  (A+(C/Ene)+(D/(Ene*Ene)));
         }


     } else {
             res= A;
     }

//     std::cout<<res<<std::endl;

    
    return res;
}

//----------

// -----------------------------------------------------------------------------------------------
// DKFJetBias provides an uncertainty for each jet
// input : TLorentzVector
//         JetFlavor : 1 or 2 (W or b)
//         IPar : parameter for which the uncertainty is computed
//              = 1, 2 or 3 (Energy, Eta or Phi)
// output : Expected bias to be subtracted from the measured observable
// Remark : This function requires AlienErrors[][][][] to be filled
//          by the function ReadAlienErrors() called once when the
//          object PyonKinFitSemi is created
// -----------------------------------------------------------------------------------------------
double PyonKinFitSemi::DKFJetBias(TLorentzVector Jet,unsigned int JetFlavor, unsigned int IPar) {

    double res=1E10;
    
    if (IPar>3) {
        std::cout<<" <!> ERROR in DKFJetBias. Unknown parameter "<<IPar<<". Parameter number is limitted to 1 -> 3"<<std::endl;
        return 1E10;
    }

    if ((AlienBiases[0][0][0][0]==0)&&(AlienBiases[0][0][0][1]==0)&&(AlienBiases[0][0][0][2]==0)&&(AlienBiases[0][0][0][3]==0)) {
        // A=B=C=D=0 for E parametrisation (Wj) in all Eta bins
        std::cout<<" <!> ERROR in PyonKinFitSemi. Errors are not loaded ! Error set to 1E10 ..."<<std::endl;
        return 1E10;
    }
    
    double A=0, B=0, C=0, D=0; // parametres pour JetW
    double Ab=0, Bb=0, Cb=0, Db=0; // parametres pour Jetb

    double Eta=Jet.Eta();
    double Ene=Jet.E();

    unsigned int ieta=0;

// 1.4 1.7 2.6
    if (fabs(Eta)<0.8) { // barrel
        ieta = 0;
    } else if ((fabs(Eta)>=0.8)&&(fabs(Eta)<1.6)) { // crak
        ieta = 1;
    } else if ((fabs(Eta)>=1.6)&&(fabs(Eta)<2.6)) { // end cap
        ieta = 2;
    } else if (fabs(Eta)>=2.6) { // tres bas angle
        ieta = 3;
    }

//    std::cout<<A<<" "<<B<<" "<<C<<" "<<D<<std::endl;
    
//     if (Ene>0) {
//         if (JetFlavor==1) {
//             res = (A+(B*sqrt(Ene))+(C/Ene)+(D/(Ene*Ene)));
//         } else if (JetFlavor==2) {
//             res = (Ab+(Bb*sqrt(Ene))+(Cb/Ene)+(Db/(Ene*Ene)));
//         }
//     } else {
//         if (JetFlavor==1) {
//             res = A;
//         } else if (JetFlavor==2) {
//              res = Ab;
//         }  
//      }

    // nouvelles (par fichier .dat)
    A=AlienBiases[IPar-1][JetFlavor-1][ieta][0];
    B=AlienBiases[IPar-1][JetFlavor-1][ieta][1];
    C=AlienBiases[IPar-1][JetFlavor-1][ieta][2];
    D=AlienBiases[IPar-1][JetFlavor-1][ieta][3];

//    std::cout<<" ipar = "<<IPar<<" flav "<<JetFlavor-1<<" ieta "<<ieta<<std::endl;
//    std::cout<<A<<" "<<B<<" "<<C<<" "<<D<<std::endl<<std::endl;

//    std::cout<<" res "<<res<<" -> ";
    
     if (Ene>0.1) {
//         res= (A+(B*sqrt(Ene))+(C/Ene)+(D/(Ene*Ene)));
         res= (A+(B*sqrt(Ene))+(C/sqrt(Ene)));

         if (IPar>1){ // eta et phi
             res =  (A+(C/Ene));
         }


     } else {
             res= A;
     }

//     std::cout<<res<<std::endl;

     // TMP TMP


     
    return res;
}


// ----



TLorentzVector PyonKinFitSemi::GetFittedJet(unsigned int iJet, unsigned int flavor) {
// returns the fitted TLorentzVector of the jet number iJet
    
    unsigned int NbJets=4; // To be put in data card (TODO)
    double jetmass;
    
    if (iJet>=NbJets) {
        std::cout<<std::endl<<" <!> Warning : Requesting fitted jet #"<<iJet<<" while only "<<NbJets<<" are fitted... "<<std::endl;
    }
    if (flavor==0) { // light jet, mass set to 0
        jetmass=0;
    } else { // if not light => b ! To be refined by adding other possiblities (TODO)
        jetmass=PDGBMass;
    }

    if (TurnJets) {
        TLorentzVector FitV(P(GetFitVar(iJet+8),jetmass)*TMath::Cos(GetFitVar(iJet+4))/TMath::CosH(GetFitVar(iJet+0)),
                            P(GetFitVar(iJet+8),jetmass)*TMath::Sin(GetFitVar(iJet+4))/TMath::CosH(GetFitVar(iJet+0)),
                            P(GetFitVar(iJet+8),jetmass)*TMath::TanH(GetFitVar(iJet+0)),GetFitVar(iJet+8));
        return FitV;
    } else {

        double tpar[16];
        
        tpar[0]=MeasEtaJ1;
        tpar[1]=MeasEtaJ2;
        tpar[2]=MeasEtaBH;
        tpar[3]=MeasEtaBL;
        tpar[4]=MeasPhiJ1;
        tpar[5]=MeasPhiJ2;
        tpar[6]=MeasPhiBH;
        tpar[7]=MeasPhiBL;
        tpar[8]= GetFitVar(0);
        tpar[9]= GetFitVar(1);
        tpar[10]= GetFitVar(2);
        tpar[11]= GetFitVar(3);
        tpar[12]= GetFitVar(4);
        tpar[13]= GetFitVar(5);
        tpar[14]= GetFitVar(6);
        tpar[15]= GetFitVar(7);
        
        TLorentzVector FitV(P(tpar[iJet+8],jetmass)*TMath::Cos(tpar[iJet+4])/TMath::CosH(tpar[iJet+0]),
                            P(tpar[iJet+8],jetmass)*TMath::Sin(tpar[iJet+4])/TMath::CosH(tpar[iJet+0]),
                            P(tpar[iJet+8],jetmass)*TMath::TanH(tpar[iJet+0]),tpar[iJet+8]);
        return FitV;
    }
}



TLorentzVector PyonKinFitSemi::GetFittedMuon() {
    // returns the fitted TLorentzVector of the muon
    
    unsigned int NbJets=4; // To be put in data card (TODO)
    unsigned int NbParJet=3;
    
    TLorentzVector FitV(GetFitVar(NbJets*NbParJet)*TMath::Cos(MeasPhiMU)/TMath::CosH(MeasEtaMU),
                             GetFitVar(NbJets*NbParJet)*TMath::Sin(MeasPhiMU)/TMath::CosH(MeasEtaMU),
                             GetFitVar(NbJets*NbParJet)*TMath::TanH(MeasEtaMU),GetFitVar(NbJets*NbParJet));
    return FitV;
}



TLorentzVector PyonKinFitSemi::GetFittedNeutrino() {
    // returns the fitted TLorentzVector of the neutrino

    unsigned int NbJets=4; // To be put in data card (TODO)
    unsigned int NbParJet=3; // 3 parameters are fitted by jet
    unsigned int NbParMuon=1; // 1 for the muon

    unsigned int FirstNeutrinoParameter = (NbJets*NbParJet)+(NbParMuon);

    if (TurnJets) {
        TLorentzVector FitV(GetFitVar(FirstNeutrinoParameter),GetFitVar(FirstNeutrinoParameter+1),GetFitVar(FirstNeutrinoParameter+2),E(0,GetFitVar(FirstNeutrinoParameter),GetFitVar(FirstNeutrinoParameter+1),GetFitVar(FirstNeutrinoParameter+2)));
        return FitV;
        
    } else {

        double tpar[16];
        
        tpar[0]=MeasEtaJ1;
        tpar[1]=MeasEtaJ2;
        tpar[2]=MeasEtaBH;
        tpar[3]=MeasEtaBL;
        tpar[4]=MeasPhiJ1;
        tpar[5]=MeasPhiJ2;
        tpar[6]=MeasPhiBH;
        tpar[7]=MeasPhiBL;
        tpar[8]= GetFitVar(0);
        tpar[9]= GetFitVar(1);
        tpar[10]= GetFitVar(2);
        tpar[11]= GetFitVar(3);
        tpar[12]= GetFitVar(4);
        tpar[13]= GetFitVar(5);
        tpar[14]= GetFitVar(6);
        tpar[15]= GetFitVar(7);

        TLorentzVector FitV(tpar[FirstNeutrinoParameter],tpar[FirstNeutrinoParameter+1],tpar[FirstNeutrinoParameter+2],E(0,tpar[FirstNeutrinoParameter],tpar[FirstNeutrinoParameter+1],tpar[FirstNeutrinoParameter+2]));
        
        return FitV;
    }
}
///////////////////////////////
// Quelques fonctions utiles

double E(double M, double PX, double PY, double PZ){
  return TMath::Sqrt(TMath::Power(M,2.)+TMath::Power(PX,2.)+TMath::Power(PY,2.)+TMath::Power(PZ,2.));
}

double P(double E, double M){
  return TMath::Sqrt(TMath::Power(E,2.)-TMath::Power(M,2.));
}

double Mass(TLorentzVector V1, TLorentzVector V2){
  return TMath::Sqrt(TMath::Power(V1.E()+V2.E(),2.)-TMath::Power(V1.Px()+V2.Px(),2.)-TMath::Power(V1.Py()+V2.Py(),2.)-TMath::Power(V1.Pz()+V2.Pz(),2.));
}

double Mass(TLorentzVector V1, TLorentzVector V2, TLorentzVector V3){
  return TMath::Sqrt(TMath::Power(V1.E()+V2.E()+V3.E(),2.)-TMath::Power(V1.Px()+V2.Px()+V3.Px(),2.)-TMath::Power(V1.Py()+V2.Py()+V3.Py(),2.)-TMath::Power(V1.Pz()+V2.Pz()+V3.Pz(),2.));
}

////////////////////////////

// ------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------
double PzNeutrinoForPyon ( double Px_l, double Py_l, double Pz_l, double Px_nu, double Py_nu, double *Pz1, double *Pz2){
  
    double a;
    double b;
    double c;

    
    double E_l = sqrt((Py_l*Py_l)+(Px_l*Px_l)+(Pz_l*Pz_l));

    // initialisation a 0
    *Pz1=0;
    *Pz2=0;

    if (E_l==0) return 0;

    double x = (-1.+(PDGMW*PDGMW/(1000*1000))+(2.*((Px_nu*Px_l)+(Py_nu*Py_l))))/(2*E_l);
        
    a=1-((Pz_l*Pz_l)/(E_l*E_l));
  
    b=-2.*(Pz_l/E_l)*x;
    
    c=((Px_nu*Px_nu)+(Py_nu*Py_nu))-(x*x);

    if (a==0) {
        if (b!=0) {
            *Pz1=-1*c/b;
            *Pz2=-1*c/b;
            return 1;
        } else {
            *Pz1=0;
            *Pz2=0;
            return 0;
        }
    }
      
    double delta;
    delta= b*b-4*a*c ;

    if (delta<0) return 0;//pas de solutions
  
    *Pz1=(-b-(sqrt(delta)))/(2*a);
    *Pz2=(-b+(sqrt(delta)))/(2*a);
    return 1;
} 

// ----------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------
double PzPtNeutrinoForPyon ( double Px_l, double Py_l, double Pz_l, double Px_nu, double Py_nu, double *Pz, double *Pt){
  
    double a;
    double b;
    double c;

    
    double Pt_nu = sqrt((Px_nu*Px_nu)+(Py_nu*Py_nu));
    double E_l = sqrt((Py_l*Py_l)+(Px_l*Px_l)+(Pz_l*Pz_l));
    *Pt=Pt_nu;
    *Pz=0;
    
    if (fabs(E_l)<0.0001) return 0;


    double x = (-1.+(PDGMW*PDGMW/(1000*1000))+(2.*((Px_nu*Px_l)+(Py_nu*Py_l))))/(2*E_l);

    
    a=1-((Pz_l*Pz_l)/(E_l*E_l));

    if (fabs(a)<0.0001) return 0;

    
    b=-2.*(Pz_l/E_l)*x;

    
    c=(Px_nu*Px_nu)-(x*x)+(Py_nu*Py_nu);
    
    double Pt1,Pt2;
    double delta;


//    *Pz=0;
    *Pz=(-b)/((2*a)+0.0001);

    
    a = (4.*((Px_nu*Px_l)+(Py_nu*Py_l))*((Px_nu*Px_l)+(Py_nu*Py_l)))-(4*E_l*E_l*Pt_nu*Pt_nu);

    a += (4*(Pz_l*Pz_l)*(Pt_nu*Pt_nu));

    b = 4*((PDGMW*PDGMW/(1000*1000))-1)*((Px_nu*Px_l)+(Py_nu*Py_l));
 
    c = ((PDGMW*PDGMW/(1000*1000))-1)*((PDGMW*PDGMW/(1000*1000))-1);

    delta= b*b+(4*a*c) ;


    if (delta>=0) {

        Pt1=(b-(sqrt(delta)))/(2*a);
        Pt2=(b+(sqrt(delta)))/(2*a);

        if (Pt1<0 && Pt2<0) { // pas de correction car negative
            *Pt=Pt_nu;
            return 0;
        } else {
            if (Pt1<0) *Pt=Pt_nu*Pt2;
            if (Pt2<0) *Pt=Pt_nu*Pt1;
            if (Pt1>0 && Pt2>0) {
                if (fabs(Pt1-1)<fabs(Pt2-1)) {
                    *Pt=Pt_nu*Pt1;
                } else {
                    *Pt=Pt_nu*Pt2;
                }
            }
        }
    
    } else {

        *Pt=Pt_nu;
        return 0;
    }

    return 1;
} 

// ----------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------
int RecoLeptSideForPyon(TLorentzVector TheMuon, TLorentzVector vMET, TLorentzVector bJet, TLorentzVector* Neutrino, double mtop_had) {

    // reconstruction du neutrino avec correction, au besoin, de la MET
    
    double Px_l=TheMuon.Px();
    double Py_l=TheMuon.Py();
    double Pz_l=TheMuon.Pz();
    
    double Px_nu=vMET.Px();
    double Py_nu=vMET.Py();
    
    double Pz1=0, Pz2=0;

    *Neutrino=vMET; // initialisation, valeur par defaut en cas d'erreur
	
    if ( PzNeutrinoForPyon ( Px_l, Py_l, Pz_l, Px_nu, Py_nu, &Pz1, &Pz2) ) {


//        std::cout<<Px_l<<" "<< Py_l<<" "<< Pz_l<<" "<< Px_nu<<" "<< Py_nu<<std::endl;
        
        // -- choix de la solution qui donne la meilleure masse de top	  
        vMET.SetPz(Pz1);
        double xE=sqrt((vMET.Px()*vMET.Px())+(vMET.Py()*vMET.Py())+(vMET.Pz()*vMET.Pz()));
        vMET.SetE(xE);

        TLorentzVector VTT(0,0,0,0);
        VTT = TheMuon;
        VTT += vMET;
        VTT += bJet;

        double mtt_1 = sqrt(fmax(0.,VTT.M2()));

        vMET.SetPz(Pz2);
        xE=sqrt((vMET.Px()*vMET.Px())+(vMET.Py()*vMET.Py())+(vMET.Pz()*vMET.Pz()));
        vMET.SetE(xE);

        TLorentzVector VTT2(0,0,0,0);
        VTT2 = TheMuon;
        VTT2 += vMET;
        VTT2 += bJet;

        double mtt_2 = sqrt(fmax(0.,VTT2.M2()));

        if (fabs(mtt_1-mtop_had) < fabs(mtt_2-mtop_had)) {
            vMET.SetPz(Pz1);
            double xE=sqrt((vMET.Px()*vMET.Px())+(vMET.Py()*vMET.Py())+(vMET.Pz()*vMET.Pz()));
            vMET.SetE(xE);
        } else {
            vMET.SetPz(Pz2);
            double xE=sqrt((vMET.Px()*vMET.Px())+(vMET.Py()*vMET.Py())+(vMET.Pz()*vMET.Pz()));
            vMET.SetE(xE);
        }


        //		    std::cout<<mtt_1<<" "<<mtt_2<<std::endl;

        *Neutrino = vMET;

    } else { // pas de solution

        double Pt1=0;
        PzPtNeutrinoForPyon ( Px_l, Py_l, Pz_l, Px_nu, Py_nu, &Pz1, &Pt1);


        double xE=vMET.Pt();
        if (xE>0) {
            vMET.SetX(vMET.Px()*Pt1/xE);
            vMET.SetY(vMET.Py()*Pt1/xE);
        }
        vMET.SetPz(Pz1);

        xE=sqrt((vMET.Px()*vMET.Px())+(vMET.Py()*vMET.Py())+(vMET.Pz()*vMET.Pz()));
        
        vMET.SetE(xE);
              
        *Neutrino = vMET;

         
    }
    return 0;
}

