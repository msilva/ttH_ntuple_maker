#include "TLorentzVector.h"
#include <vector>
#ifdef __MAKECINT__
#pragma link C++ class vector<TLorentzVector>;
#endif

void jet_maker(double pt_cut, double eta_cut, 
	       vector<float>  *m_jet_pt, vector<float>  *m_jet_eta, vector<float>  *m_jet_phi, vector<float>  *m_jet_m,
	       Float_t & mass_jet, Float_t & HT_jet,
	       Int_t & N_jet, Int_t & N_jet_cen, Int_t & N_jet_fwd,
	       vector<char>  *bjet_70, vector<double>  *bjet_score, Int_t & N_bjet_70,
	       vector<float> &jet_pt, vector<float> &jet_eta, vector<float> &jet_phi, vector<float> &jet_E,
	       vector<float> &jet_raw_score, vector<int> &jet_pseudo_score,
	       std::vector< TLorentzVector > & recoJets, vector < int > & isBTagged_70){
  
  //actually run through the loop
  N_jet = 0, N_jet_cen = 0, N_jet_fwd = 0, N_bjet_70 = 0;
  mass_jet = 0, HT_jet = 0; 
  recoJets.clear(); isBTagged_70.clear();

  TLorentzVector all_jets;
  for(int n_jet = 0; n_jet < m_jet_pt->size(); n_jet++){
    //first apply standard pt and eta cuts
    if(fabs((*m_jet_eta)[n_jet]) > eta_cut) continue;
    if((*m_jet_pt)[n_jet] < pt_cut) continue;

    //then we count as a jet
    N_jet++;
    HT_jet   += (*m_jet_pt)[n_jet];

    //sum all jets to get invariant mass
    TLorentzVector this_jet;
    this_jet.SetPtEtaPhiM((*m_jet_pt)[n_jet], (*m_jet_eta)[n_jet], (*m_jet_phi)[n_jet], (*m_jet_m)[n_jet]);
    all_jets += this_jet;
    recoJets.push_back(this_jet);
    isBTagged_70.push_back(bool((*bjet_70)[n_jet]));

    //then apply pseudorapidity cuts
    if(fabs((*m_jet_eta)[n_jet]) < 2.5)
      N_jet_cen ++;
    else
      N_jet_fwd ++;

    //now apply bjet cuts
    if(bool((*bjet_70)[n_jet])) N_bjet_70++;

    //make vectors for all jets
    int pseudo_score;
    if     ( (*bjet_score)[n_jet] > 0.934906 ) pseudo_score = 1;
    else if( (*bjet_score)[n_jet] > 0.8244273) pseudo_score = 2;
    else if( (*bjet_score)[n_jet] > 0.645925 ) pseudo_score = 3;
    else if( (*bjet_score)[n_jet] > 0.1758475) pseudo_score = 4;
    else if( (*bjet_score)[n_jet] > -1.000000) pseudo_score = 5;
    else                                       pseudo_score = -999;

    jet_pt.push_back(this_jet.Pt() );
    jet_eta.push_back(this_jet.Eta() );
    jet_phi.push_back(this_jet.Phi() );
    jet_E.push_back(this_jet.E() );
    jet_raw_score.push_back( (float) (*bjet_score)[n_jet] );
    jet_pseudo_score.push_back( pseudo_score);
  }
  mass_jet = all_jets.M();

}

//our method for extracting mt and pt for lepton+MET
void pt_mt_lep_met_comp(Float_t & pt, Float_t & mt,
			Float_t met, Float_t phi_met,
			vector<float>  *electron_pt, vector<float>  *electron_eta, vector<float>  *electron_phi, 
			vector<float>  *electron_charge,
			vector<float>  *muon_pt,     vector<float>  *muon_eta,     vector<float>  *muon_phi,
			vector<float>  *muon_charge,
			vector<float>  &lepton_pt, vector<float>  &lepton_eta, vector<float>  &lepton_phi, vector<float>  &lepton_E, 
			vector<float>  &lepton_charge, vector<int>  &lepton_type){
  
  if( (electron_pt->size() == 0) && (muon_pt->size() == 0) ){
    pt = 0; mt = 0; return;}
  
  //initialize vectors
  TLorentzVector metvec;
  TLorentzVector lepvec;
  TLorentzVector systvec;
  
  //make MET vector
  metvec.SetPtEtaPhiE(met, 0, phi_met, met);
  systvec += metvec;
  
  //make lep vector
  //use leading lepton
  bool useEl = (electron_pt->size() > 0);
  if ( useEl && (muon_pt->size() > 0) ) {
    useEl = ((*electron_pt)[0] > (*muon_pt)[0]);}
  
  if (useEl) lepvec.SetPtEtaPhiM((*electron_pt)[0], (*electron_eta)[0], (*electron_phi)[0], 0.5109989461);
  else       lepvec.SetPtEtaPhiM((*muon_pt)[0],     (*muon_eta)[0],     (*muon_phi)[0],     105.6583745);
  
  systvec+= lepvec;
  pt = systvec.Pt();
  mt = sqrt( 2 * fabs(metvec.Pt()) * fabs(lepvec.Pt()) * (1-cos(metvec.DeltaPhi(lepvec))) );
  
  lepton_pt.push_back( lepvec.Pt() );
  lepton_eta.push_back( lepvec.Eta() );
  lepton_phi.push_back( lepvec.Phi() );
  lepton_E.push_back( lepvec.E() );
  if (useEl) lepton_charge.push_back( (*electron_charge)[0] );
  else       lepton_charge.push_back( (*muon_charge)[0] );
  if (useEl) lepton_type.push_back( 1 );
  else       lepton_type.push_back( 0 );
  
  return;
}

Float_t get_norm(TFile *f_name, TString s_name){
  s_name.ReplaceAll("mc15c/mc15c.","");
  s_name.ReplaceAll("../h015/","");
  s_name.ReplaceAll(".MxAODDetailed.p2666.h015d.root","");
  s_name.ReplaceAll(".MxAODDetailed.p2908.h015d.root","");
  s_name.ReplaceAll(".MxAODDetailed.p2952.h015d.root","");
  s_name.ReplaceAll(".MxAODDetailed.p3015.h015d.root","");
  s_name.ReplaceAll(".MxAODDetailed.p3075.h015d.root","");
  s_name.ReplaceAll(".MxAODDetailed.p2908.h015.root","");
  s_name.ReplaceAll(".MxAODDetailed.p2952.h015.root","");
  s_name.ReplaceAll(".MxAODDetailed.p3015.h015.root","");
  s_name.ReplaceAll(".MxAODDetailed.p3075.h015.root","");
  TString new_name = "CutFlow_"+s_name+"_weighted";
  TH1F* h_norm = (TH1F*)f_name->Get(new_name);
  Float_t norm = h_norm->GetBinContent(1)* h_norm->GetBinContent(3)/h_norm->GetBinContent(2);
  return 1/norm;
}
