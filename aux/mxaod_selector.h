#include "TLorentzVector.h"
#include <vector>
#ifdef __MAKECINT__
#pragma link C++ class vector<TLorentzVector>;
#endif

void jet_selector(double pt_cut, double eta_cut, 
		  vector<float>  *jet_pt, vector<float>  *jet_eta, vector<float>  *jet_phi, vector<float>  *jet_m,
		  Float_t & mass_jet, Float_t & HT_jet, Float_t & dR_jj_closest, Float_t & dR_jj_leading, 
		  Int_t & N_jet, Int_t & N_jet_cen, Int_t & N_jet_fwd,
		  vector<char>  *bjet_60, vector<char>  *bjet_70,
		  vector<char>  *bjet_77, vector<char>  *bjet_85,
		  Int_t & N_bjet_60, Int_t & N_bjet_70, 
		  Int_t & N_bjet_77, Int_t & N_bjet_85,
		  std::vector< TLorentzVector > & recoJets,
		  vector < int > & isBTagged_60, vector < int > & isBTagged_70,
		  vector < int > & isBTagged_77, vector < int > & isBTagged_85){
  
  //actually run through the loop
  N_jet = 0, N_jet_cen = 0, N_jet_fwd = 0;
  mass_jet = 0, HT_jet = 0; dR_jj_closest = 0; dR_jj_leading = 0;
  N_bjet_60 = 0, N_bjet_70 = 0, N_bjet_77 = 0, N_bjet_85 = 0;
  recoJets.clear();
  isBTagged_60.clear(); isBTagged_70.clear();
  isBTagged_77.clear(); isBTagged_85.clear();
  TLorentzVector all_jets;
  double lead_pt =0, sublead_pt = 0;
  TLorentzVector lead_jet, sublead_jet;
  double minimize_dR = 10000000; //some ridiculous
  for(int n_jet = 0; n_jet < jet_pt->size(); n_jet++){
    //first apply standard pt and eta cuts
    if(fabs((*jet_eta)[n_jet]) > eta_cut) continue;
    if((*jet_pt)[n_jet] < pt_cut) continue;
    
    //then we count as a jet
    N_jet++;
    HT_jet   += (*jet_pt)[n_jet];

    //sum all jets to get invariant mass
    TLorentzVector this_jet;
    this_jet.SetPtEtaPhiM((*jet_pt)[n_jet], (*jet_eta)[n_jet], (*jet_phi)[n_jet], (*jet_m)[n_jet]);
    all_jets += this_jet;

    //then apply pseudorapidity cuts
    if(fabs((*jet_eta)[n_jet]) < 2.5)
      N_jet_cen ++;
    else
      N_jet_fwd ++;
    
    //now apply bjet cuts
    if(bool((*bjet_60)[n_jet])) N_bjet_60++;
    if(bool((*bjet_70)[n_jet])) N_bjet_70++;
    if(bool((*bjet_77)[n_jet])) N_bjet_77++;
    if(bool((*bjet_85)[n_jet])) N_bjet_85++;
    
    //construct vector for KinFit now
    recoJets.push_back(this_jet);
    isBTagged_60.push_back(bool((*bjet_60)[n_jet])); isBTagged_70.push_back(bool((*bjet_70)[n_jet]));
    isBTagged_77.push_back(bool((*bjet_77)[n_jet])); isBTagged_85.push_back(bool((*bjet_85)[n_jet]));
    
    //for deltaR leading jets
    if     ((*jet_pt)[n_jet] > lead_pt)   {
      lead_pt = (*jet_pt)[n_jet];
      lead_jet.SetPtEtaPhiM((*jet_pt)[n_jet], (*jet_eta)[n_jet], (*jet_phi)[n_jet], (*jet_m)[n_jet]);}
    else if((*jet_pt)[n_jet] > sublead_pt){ 
      sublead_pt = (*jet_pt)[n_jet];
      sublead_jet.SetPtEtaPhiM((*jet_pt)[n_jet], (*jet_eta)[n_jet], (*jet_phi)[n_jet], (*jet_m)[n_jet]);}
    dR_jj_leading = lead_jet.DeltaR(sublead_jet);
    
    //deltaR for closest jets, eg minimize dR
    for(int n_jet2 = n_jet+1; n_jet2 < jet_pt->size(); n_jet2++){
      if(fabs((*jet_eta)[n_jet2]) > eta_cut) continue;
      if((*jet_pt)[n_jet2] < pt_cut) continue;
      TLorentzVector second_jet;
      second_jet.SetPtEtaPhiM((*jet_pt)[n_jet2], (*jet_eta)[n_jet2], (*jet_phi)[n_jet2], (*jet_m)[n_jet2]);
      if(second_jet.DeltaR(this_jet) < minimize_dR) minimize_dR = second_jet.DeltaR(this_jet);
    }
    dR_jj_closest = minimize_dR;
  }
  mass_jet = all_jets.M();
}

//our method for extracting mt and pt for lepton+MET
void pt_mt_lep_met_comp(Float_t & pt, Float_t & mt,
			Float_t met, Float_t phi_met,
			vector<float>  *electron_pt, vector<float>  *electron_eta, vector<float>  *electron_phi,
			vector<float>  *muon_pt,     vector<float>  *muon_eta,     vector<float>  *muon_phi){
  
  if( (electron_pt->size() == 0) && (muon_pt->size() == 0) ){
    pt = 0; mt = 0; return;}
  
  //initialize vectors
  TLorentzVector metvec;
  TLorentzVector lepvec;
  TLorentzVector systvec;

  //make MET vector
  metvec.SetPtEtaPhiE(met, 0, phi_met, met);
  systvec += metvec;
  
  //make lep vector
  //use leading lepton
  bool useEl = (electron_pt->size() > 0);
  if ( useEl && (muon_pt->size() > 0) ) {
    useEl = ((*electron_pt)[0] > (*muon_pt)[0]);}
  
  if (useEl) lepvec.SetPtEtaPhiM((*electron_pt)[0], (*electron_eta)[0], (*electron_phi)[0], 0.5109989461);
  else       lepvec.SetPtEtaPhiM((*muon_pt)[0],     (*muon_eta)[0],     (*muon_phi)[0],     105.6583745);
  
  //compute transverse plane momentum and mass
  /*cout << "TLorentzVector metvec" << endl;
  cout << "metvec.SetPtEtaPhiE(" << met << ", " << 0 << ", " << phi_met << ", " << met << ")" << endl;
  cout << "TLorentzVector lepvec" << endl;
  if (useEl) 
  cout << "lepvec.SetPtEtaPhiE(" << (*electron_pt)[0] << ", " << (*electron_eta)[0] << ", " << (*electron_phi)[0] << ", " << 0.5109989461 << ")" << endl;
  else
  cout << "lepvec.SetPtEtaPhiE(" << (*muon_pt)[0] << ", " << (*muon_eta)[0] << ", " << (*muon_phi)[0] << ", " << 105.6583745 << ")" << endl;
  cout << "TLorentzVector systvec" << endl;
  */
  systvec+= lepvec;
  pt = systvec.Pt();
  //mt = systvec.Mt();
  mt = sqrt( 2 * fabs(metvec.Pt()) * fabs(lepvec.Pt()) * (1-cos(metvec.DeltaPhi(lepvec))) );
  
  return;
}

//method used internal to mxaods
void pt_mt_lep_met_comp_mxaod(Float_t & pt, Float_t & mt,
			      Float_t met, Float_t phi_met,
			      vector<float>  *electron_pt, vector<float>  *electron_eta, vector<float>  *electron_phi,
			      vector<float>  *muon_pt,     vector<float>  *muon_eta,     vector<float>  *muon_phi){
  
  if( (electron_pt->size() == 0) && (muon_pt->size() == 0) ){
    pt = 0; mt = 0; return;}
  
  //initialize vectors
  TLorentzVector metvec;
  TLorentzVector lepvec;
  TLorentzVector systvec;
  
  //make MET vector
  metvec.SetPtEtaPhiE(met, 0, phi_met, 0);
  systvec += metvec;
  
  //make lep vector
  //use leading lepton
  bool useEl = (electron_pt->size() > 0);
  if ( useEl && (muon_pt->size() > 0) ) {
    useEl = ((*electron_pt)[0] > (*muon_pt)[0]);}
  
  if (useEl) lepvec.SetPtEtaPhiM((*electron_pt)[0], (*electron_eta)[0], (*electron_phi)[0], 0.5109989461);
  else       lepvec.SetPtEtaPhiM((*muon_pt)[0],     (*muon_eta)[0],     (*muon_phi)[0],     105.6583745);
  
  //compute transverse plane momentum and mass
  systvec+= lepvec;
  pt = systvec.Pt();
  mt = systvec.M();
  
  return;
}

Float_t get_norm(TFile *f_name, TString s_name){
  s_name.ReplaceAll("mc15c/mc15c.","");
  s_name.ReplaceAll(".MxAODDetailed.p2908.h015d.root","");
  s_name.ReplaceAll(".MxAODDetailed.p2952.h015d.root","");
  s_name.ReplaceAll(".MxAODDetailed.p3015.h015d.root","");
  s_name.ReplaceAll(".MxAODDetailed.p3075.h015d.root","");
  TString new_name = "CutFlow_"+s_name+"_weighted";
  TH1F* h_norm = (TH1F*)f_name->Get(new_name);
  Float_t norm = h_norm->GetBinContent(1)* h_norm->GetBinContent(3)/h_norm->GetBinContent(2);
  return 1/norm;
}
