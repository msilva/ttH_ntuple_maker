#!/bin/bash

i=$1
startEvent=$2
lastEvent=$3
w_dir=${PWD}
tree_dir=/eos/atlas/unpledged/group-wisc/users/msilva/local_jobs/temp_ttrees

echo "setting up the job with settings"
echo "sample" $i
echo "starting" $startEvent
echo "stopping" $lastEvent
echo "in dir"
echo $w_dir
echo "bsub -G ATLASWISC_GEN -q wisc -o ~/bsub/result_"${i}"_"$((${startEvent}/100))" /afs/cern.ch/work/m/msilva/machine_learning/make_ntuples/scripts/run_job.sh "${i} $startEvent $lastEvent

#mkdir $w_dir
#cd $w_dir
cp -rf /afs/cern.ch/work/m/msilva/machine_learning/make_ntuples/macros $w_dir
cp -rf /afs/cern.ch/work/m/msilva/machine_learning/make_ntuples/aux    $w_dir
cp -rf /afs/cern.ch/work/m/msilva/machine_learning/make_ntuples/Auto*  $w_dir
mkdir root_files

#actually run jobs
export EOS_MGM_URL=root://eosatlas.cern.ch
export PATH=/afs/cern.ch/project/eos/installation/0.3.84-aquamarine/bin:$PATH
source /afs/cern.ch/project/eos/installation/atlas/etc/setup.sh
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:`pwd`/lib:/afs/cern.ch/sw/lcg/external/Boost/1.55.0_python2.7/x86_64-slc6-gcc47-opt/lib
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Gcc/gcc484_x86_64_slc6/slc6/gcc48/setup.sh
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/5.34.32-HiggsComb-p1-x86_64-slc6-gcc48-opt/bin/thisroot.sh

root -l -b -q "macros/make_RNN_TTree.C(${i}, ${startEvent}, ${lastEvent})"

#file management
mkdir ${tree_dir}
mv root_files/*.root  ${tree_dir}
