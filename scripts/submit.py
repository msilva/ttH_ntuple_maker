#!/usr/bin/env python

import os
import sys
input1 = sys.argv[1]

eventsPerJob=100

nEvents={1: 2662018, 2: 2116178, 3: 6275519, 4: 7416976, 5: 4939376, 6: 1872385, 7: 5232896, 8: 984535, 9: 467067, 10: 105894, 11: 215026, 12: 1149144, 13: 48972, 14: 31604}

for i in nEvents:
    if int(i) != int(input1):
        continue
    for step in range(nEvents[i]/eventsPerJob):
        startEvent = step * eventsPerJob
        endEvent = (step + 1) * eventsPerJob
        if endEvent > nEvents[i]: endEvent = nEvents[i]
        command = ' bsub -G ATLASWISC_GEN -q wisc -o ~/bsub/result_%s_%s /afs/cern.ch/work/m/msilva/machine_learning/make_ntuples/scripts/run_job.sh %s %s %s' % (i, step, i, startEvent, endEvent)
        print(command)
        os.system(command)
        #status, output = commands.getstatusoutput(command)
        # to handle the tail
    if endEvent < nEvents[i]:
        startEvent = endEvent
        endEvent = nEvents[i]
        command = ' bsub -G ATLASWISC_GEN -q wisc -o ~/bsub/result_%s_%s /afs/cern.ch/work/m/msilva/machine_learning/make_ntuples/scripts/run_job.sh %s %s %s' % (i, step, i, startEvent, endEvent)
        print(command)
        os.system(command)
        #status, output = commands.getstatusoutput(command)

