#directory where the outputs of submit.py are deposited, eg tree_dir
w_dir=/eos/atlas/unpledged/group-wisc/users/msilva/local_jobs/temp_ttrees
#directory where this package is located
main_dir=$PWD/root_files


#first hadd the data files together
hadd ${main_dir}/data15.root ${w_dir}/data15_*.root
hadd ${main_dir}/data16_DS1.root ${w_dir}/data16_DS1_*.root
hadd ${main_dir}/data16_DS2.root ${w_dir}/data16_DS2_*.root
hadd ${main_dir}/data16_DS3.root ${w_dir}/data16_DS3_*.root
hadd ${main_dir}/data16_DS4.root ${w_dir}/data16_DS4_*.root
hadd ${main_dir}/data16_DS5.root ${w_dir}/data16_DS5_*.root
hadd ${main_dir}/data16_DS6.root ${w_dir}/data16_DS6_*.root
hadd ${main_dir}/data_RNN.root ${main_dir}/data16_*.root ${main_dir}/data15_*.root

#then hadd the MC files together
hadd ${main_dir}/ggH.root ${w_dir}/ggH_*.root
hadd ${main_dir}/VBF.root ${w_dir}/VBF_*.root
hadd ${main_dir}/WH.root  ${w_dir}/WmH_*.root ${w_dir}/WpH_*.root
hadd ${main_dir}/ZH.root  ${w_dir}/ZH_*.root
hadd ${main_dir}/ttH.root ${w_dir}/ttH_*.root
hadd ${main_dir}/tWH_plus1.root ${w_dir}/tWH_plus1_*.root
hadd ${main_dir}/tHjb_plus1.root ${w_dir}/tHjb_plus1_*.root